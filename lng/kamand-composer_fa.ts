<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fa_IR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../frm/aboutdialog.ui" line="14"/>
        <source>About Kamand Coomposer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/aboutdialog.ui" line="24"/>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/aboutdialog.ui" line="29"/>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/aboutdialog.cpp" line="20"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/aboutdialog.cpp" line="23"/>
        <source>&amp;Licenses</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="../frm/aboutwidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/aboutwidget.cpp" line="25"/>
        <source>Kamand Composer </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorPickerWidget</name>
    <message>
        <location filename="../src/gui/cpw/colorpickerwidget.cpp" line="51"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/cpw/colorpickerwidget.cpp" line="53"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/cpw/colorpickerwidget.cpp" line="55"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/cpw/colorpickerwidget.cpp" line="57"/>
        <source>Tonal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/cpw/colorpickerwidget.cpp" line="111"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/cpw/colorpickerwidget.cpp" line="137"/>
        <source>Custom Color...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditorWidget</name>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="141"/>
        <source>1 - Commands available to users</source>
        <translation>۱ - فرمان‌های در دسترس کاربر</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="144"/>
        <source>2 - Unix and C system calls</source>
        <translation>۲ - فراخوانی‌های سیستمی Unix و C</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="147"/>
        <source>3 - C library routines for C programs</source>
        <translation>۳ - روال‌های کتابخانه‌ای C برای برنامه‌های C</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="150"/>
        <source>4 - Special file names</source>
        <translation>۴ - نام فایل‌های خاص</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="153"/>
        <source>5 - File formats and conventions for files used by Unix</source>
        <translation>۵ - قالب‌های فابل و قراردادها برای فایل‌های مورد استفاده Unix</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="156"/>
        <source>6 - Games</source>
        <translation>۶ - بازی‌ها</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="159"/>
        <source>7 - Word processing packages</source>
        <translation>۷ - بسته‌های واژه‌پرداز</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="162"/>
        <source>8 - System administration commands and procedures</source>
        <translation>۸ - فرمان‌ها و روندهای مدیریت سیستم</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="13"/>
        <source>&amp;Section</source>
        <translation>بخش</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="16"/>
        <source>&amp;Name</source>
        <translation>نام</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="19"/>
        <source>S&amp;ynopsis</source>
        <translation>چکیده</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="22"/>
        <source>&amp;Description</source>
        <translation>توضیح</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="25"/>
        <source>&amp;Return Value</source>
        <translation>مقدار بازگشتی</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="28"/>
        <source>&amp;Errors</source>
        <translation>خطاها</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="31"/>
        <source>A&amp;ttributes</source>
        <translation>ویژگی‌ها</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="34"/>
        <source>&amp;Author</source>
        <translation>نویسنده</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="37"/>
        <source>&amp;Conforming To</source>
        <translation>مطابق با</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="40"/>
        <source>&amp;Notes</source>
        <translation>ملاحظات</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="43"/>
        <source>&amp;Bugs</source>
        <translation>اشکالات</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="46"/>
        <source>Re&amp;porting Bugs</source>
        <translation>گزارش اشکالات</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="49"/>
        <source>&amp;See Also</source>
        <translation>همچنین ببینید</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="52"/>
        <source>Co&amp;lophon</source>
        <translation>پایان‌نگاشت</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="55"/>
        <source>Copyri&amp;ght</source>
        <translation>حق طبع و نشر</translation>
    </message>
    <message>
        <location filename="../src/gui/ew/editorwidget_tr.cpp" line="58"/>
        <source>&amp;Keywords</source>
        <translation>کلیدواژه‌ها</translation>
    </message>
</context>
<context>
    <name>KeywordsWidget</name>
    <message>
        <location filename="../src/gui/keywordswidget.cpp" line="23"/>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;Insert Keyword (Insert)&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/keywordswidget.cpp" line="33"/>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;Add Keyword (Ctrl++)&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/keywordswidget.cpp" line="43"/>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;Delete Keyword (Delete)&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/keywordswidget.cpp" line="56"/>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;Move Keyword Up (Ctrl+UpArrow)&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/keywordswidget.cpp" line="66"/>
        <source>&lt;p style=&apos;white-space:pre&apos;&gt;Move Keyword Down (Ctrl+DownArrow)&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LicensesWidget</name>
    <message>
        <location filename="../frm/licenseswidget.ui" line="14"/>
        <source>Licences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frm/mainwindow.ui" line="14"/>
        <source>Kamand Composer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="28"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="32"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="39"/>
        <source>Save &amp;As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="57"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="61"/>
        <source>&amp;Fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="92"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="101"/>
        <source>For&amp;mat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="105"/>
        <source>&amp;Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="116"/>
        <source>&amp;Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="123"/>
        <source>&amp;Paragraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="137"/>
        <source>&amp;Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="148"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="156"/>
        <source>&amp;Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="160"/>
        <source>&amp;List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="196"/>
        <source>&amp;Save...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="199"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="208"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="211"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="219"/>
        <source>&amp;Author</source>
        <translation type="unfinished">نوی&amp;سنده</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="227"/>
        <source>&amp;Return Value</source>
        <translation type="unfinished">&amp;مقدار بازگشتی</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="235"/>
        <source>&amp;Errors</source>
        <translation type="unfinished">&amp;خطاها</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="243"/>
        <source>A&amp;ttributes</source>
        <translation type="unfinished">&amp;ویژگی‌ها</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="251"/>
        <source>&amp;Conforming To</source>
        <translation type="unfinished">م&amp;طابق با</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="259"/>
        <source>&amp;Notes</source>
        <translation type="unfinished">م&amp;لاحظات</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="267"/>
        <source>&amp;Bugs</source>
        <translation type="unfinished">&amp;اشکالات</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="275"/>
        <source>Re&amp;porting Bugs</source>
        <translation type="unfinished">&amp;گزارش اشکالات</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="283"/>
        <source>&amp;See Also</source>
        <translation type="unfinished">&amp;همچنین ببینید</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="291"/>
        <source>Co&amp;lophon</source>
        <translation type="unfinished">&amp;پایان‌نگاشت</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="299"/>
        <source>Copyri&amp;ght</source>
        <translation type="unfinished">&amp;حق طبع و نشر</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="311"/>
        <source>&amp;Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="314"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="326"/>
        <source>&amp;Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="329"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="341"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="344"/>
        <source>Ctrl+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="356"/>
        <source>Stri&amp;kethough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="359"/>
        <source>Ctrl+K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="368"/>
        <source>&amp;Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="371"/>
        <location filename="../frm/mainwindow.ui" line="386"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="374"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="383"/>
        <source>From &amp;Template...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="389"/>
        <source>Ctrl+Shift+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="398"/>
        <source>&amp;Remove Formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="401"/>
        <source>Ctrl+Alt+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="410"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="413"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="422"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="425"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="430"/>
        <source>C&amp;lose All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="433"/>
        <source>Ctrl+Shift+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="442"/>
        <source>Save &amp;All...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="445"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="454"/>
        <source>&amp;Document...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="457"/>
        <location filename="../frm/mainwindow.ui" line="472"/>
        <source>Save As</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="460"/>
        <source>Ctrl+Alt+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="469"/>
        <source>&amp;Template...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="484"/>
        <source>&amp;Full Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="487"/>
        <source>F11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="496"/>
        <source>&amp;Text...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="499"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="508"/>
        <source>&amp;Highlight...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="511"/>
        <location filename="../src/gui/mainwindow_toolbar.cpp" line="26"/>
        <source>Highlight Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="520"/>
        <source>&amp;Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="525"/>
        <source>&amp;About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="530"/>
        <source>About &amp;Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="538"/>
        <source>&amp;Tabbed View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="543"/>
        <source>&amp;Tile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="548"/>
        <source>&amp;Cascade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="553"/>
        <source>Tile &amp;Horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="558"/>
        <source>Tile &amp;Vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="567"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="570"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="579"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="582"/>
        <source>Ctrl+Shift+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="591"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="594"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="603"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="606"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="615"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="618"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="626"/>
        <source>&amp;Keywords</source>
        <translation type="unfinished">کلیدواژه‌ها</translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="638"/>
        <source>&amp;Left-aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="641"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="653"/>
        <source>&amp;Right-aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="656"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="668"/>
        <source>Left &amp;to Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="671"/>
        <source>Ctrl+Shift+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="683"/>
        <source>Right t&amp;o Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="686"/>
        <source>Ctrl+Shift+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="695"/>
        <source>&amp;Table...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="708"/>
        <source>&amp;Hyperlink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="720"/>
        <source>&amp;Menu Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="723"/>
        <source>F8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="735"/>
        <source>&amp;Tool Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="738"/>
        <source>F7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="747"/>
        <source>&amp;Buletted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mainwindow.ui" line="756"/>
        <source>&amp;Numbered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_toolbar.cpp" line="21"/>
        <source>Font Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_toolbar.cpp" line="22"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_toolbar.cpp" line="27"/>
        <source>No Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="140"/>
        <location filename="../src/gui/mainwindow_menu.cpp" line="194"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="141"/>
        <location filename="../src/gui/mainwindow_menu.cpp" line="195"/>
        <source>Please assign a name to this document to be able to save it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="163"/>
        <location filename="../src/gui/mainwindow_menu.cpp" line="213"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="164"/>
        <location filename="../src/gui/mainwindow_menu.cpp" line="214"/>
        <source>At least one of the files you are going to save already exists.
 Replace?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="343"/>
        <source>Remove Hyperlink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="344"/>
        <source>Insert Hyperlink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mainwindow_menu.cpp" line="567"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MdiChild</name>
    <message>
        <location filename="../frm/mdichild.ui" line="27"/>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/mdichild.ui" line="32"/>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mdichild.cpp" line="47"/>
        <source>En&amp;glish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/mdichild.cpp" line="48"/>
        <source>&amp;Persian</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefsDlg</name>
    <message>
        <location filename="../frm/prefsdlg.ui" line="14"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefsGeneralWidget</name>
    <message>
        <location filename="../frm/prefsgeneralwidget.ui" line="14"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/prefsgeneralwidget.ui" line="20"/>
        <source>Always ask for confirmation upon e&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefsUiWidget</name>
    <message>
        <location filename="../frm/prefsuiwidget.ui" line="14"/>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/prefsuiwidget.ui" line="22"/>
        <source>&amp;English Multiline Text Editors&apos; Font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/prefsuiwidget.ui" line="36"/>
        <location filename="../frm/prefsuiwidget.ui" line="95"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/prefsuiwidget.ui" line="51"/>
        <location filename="../frm/prefsuiwidget.ui" line="110"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The quick brown fox jumps over the lazy dog.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/prefsuiwidget.ui" line="58"/>
        <location filename="../frm/prefsuiwidget.ui" line="117"/>
        <source>Type your sample text here...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/prefsuiwidget.ui" line="81"/>
        <source>&amp;Persian Multiline Text Editors&apos; Font...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableMakerWidget</name>
    <message>
        <location filename="../src/gui/tbl/tablemakerwidget.cpp" line="16"/>
        <source>&lt;b&gt;Table&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/tbl/tablemakerwidget.cpp" line="20"/>
        <source>More Options...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TablePropsDialog</name>
    <message>
        <location filename="../frm/tablepropsdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/tablepropsdialog.ui" line="22"/>
        <source>&amp;Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frm/tablepropsdialog.ui" line="42"/>
        <source>C&amp;olumns</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpDownButtonsWidget</name>
    <message>
        <location filename="../src/gui/updownbuttonswidget.cpp" line="65"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/updownbuttonswidget.cpp" line="76"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
