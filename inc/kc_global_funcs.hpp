#ifndef KC_GLOBAL_FUNCS_HPP
#define KC_GLOBAL_FUNCS_HPP

#include <QString>

#include "kc_global_types.hpp"

class MainWindow;

void onAboutToQuit(MainWindow* w);
QString fieldToNodeName(Field f);
Field nodeNameToField(const QString& name);

#endif // KC_GLOBAL_FUNCS_HPP
