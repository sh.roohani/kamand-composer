#ifndef KC_CONFIG_HPP
#define KC_CONFIG_HPP

#include <QString>
#include <QList>

#include "gui/cpw/colors_globals.hpp"

class QSettings;

class Config
{
public:
	Config(const QString& path);
	Config();
	~Config();
	Config(const Config& other) = delete;

	Config& operator=(const Config& rhs) = delete;

	inline const QString& path() const { return m_path; }
	inline void setPath(const QString& path) { m_path	= path; }

	bool load();
	bool save();

	QByteArray							m_mwGeometry,
													m_mwState;
	bool										m_fullscreen				{ false };

	QString									m_lastDocDir,
													m_lastTmplDir;

	QList<ColorProperties>	m_txtCustomColors,
													m_hltCustomColors,
													m_txtRecentColors,
													m_hltRecentColors;

	bool										m_tabbedView				{ false },
													m_exitConfirmation  { true };
	QString									m_enMlteFont,
													m_faMlteFont;

private:
	QString		m_path;
};

#endif // KC_CONFIG_HPP
