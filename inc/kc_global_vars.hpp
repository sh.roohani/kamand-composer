#ifndef KC_GLOBAL_VARS_HPP
#define KC_GLOBAL_VARS_HPP

#include <QTranslator>

#include "kc_config.hpp"

extern Config				*g_cfg;
extern QTranslator	g_faTr;

#endif // KC_GLOBAL_VARS_HPP
