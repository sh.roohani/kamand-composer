#ifndef KC_GLOBAL_TYPES_HPP
#define KC_GLOBAL_TYPES_HPP

#include <QtCore>

#define	KC_ORG_NAME					"LinuxForLife"
#define	KC_ORG_DOMAIN				"linuxforlife.ir"
#define	KC_APP_NAME					"Kamand Composer"

#define KC_EXT_DOCUMENT			".kml"
#define KC_EXT_TEMPLATE			".kct"

#define KC_FILTER_DOCUMENTS	KC_APP_NAME " Documents (*" KC_EXT_DOCUMENT ")"
#define KC_FILTER_TEMPLATES	KC_APP_NAME " Documents (*" KC_EXT_TEMPLATE ")"
#define KC_FILTER_ALL_FILES	"All Files (*.*)"

#define KC_VER_MAJOR		0
#define KC_VER_MINOR		5
#define KC_VER_PATCH		22

#define KC_TAG_DOC						"doc"
#define KC_TAG_BLOCK					"block"
#define KC_TAG_FRAG						"fragment"
#define KC_TAG_KEYWORD				"keyword"

#define KC_ATTR_ALIGN					"align"
#define KC_ATTR_DIR						"dir"
#define KC_ATTR_BOLD					"bold"
#define KC_ATTR_ITALIC				"italic"
#define KC_ATTR_UNDERLINE			"underline"
#define KC_ATTR_STRIKETHROUGH	"strikethrough"
#define KC_ATTR_TXT_COLOR			"txt-color"
#define KC_ATTR_HLT_COLOR			"hlt-color"

#define KC_VAL_LEFT						"left"
#define KC_VAL_RIGHT					"right"
#define KC_VAL_LTR						"ltr"
#define KC_VAL_RTL						"rtl"
#define KC_VAL_TRUE						"true"
#define KC_VAL_FALSE					"false"

enum class Field
{
	Unknown,
	Section,
	Name,
	Synopsis,
	Description,
	ReturnValue,
	Author,
	Errors,
	Attributes,
	ConformingTo,
	Notes,
	Bugs,
	ReportingBugs,
	SeeAlso,
	Colophon,
	Copyright,
	Keywords,
	Last,
};
Q_DECLARE_METATYPE(Field)

enum class Section
{
	Unknown,
	Section1,
	Section2,
	Section3,
	Section4,
	Section5,
	Section6,
	Section7,
	Section8,
	Last,
};
Q_DECLARE_METATYPE(Section)

enum class Language
{
	Unknown,
	English,
	Persian,
	Last,
};
Q_DECLARE_METATYPE(Language)

// Postfix
Field operator++(Field& f, int);
// Prefix
Field& operator++(Field& f);
// Postfix
Section operator++(Section& s, int);
// Prefix
Section& operator++(Section& s);

#endif // KC_GLOBAL_TYPES_HPP
