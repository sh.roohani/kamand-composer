#ifndef KEYWORKDSWIDGET_HPP
#define KEYWORKDSWIDGET_HPP

#include <QWidget>

class QHBoxLayout;
class QVBoxLayout;
class QListWidget;
class QToolButton;

class KeywordsWidget : public QWidget
{
	Q_OBJECT
public:
	explicit KeywordsWidget(QWidget *parent = nullptr);

	QStringList keywords();
	void setKeywords(const QStringList& kwList, bool doSignal = false);

private:
	QHBoxLayout	*m_hboxLayout	{ nullptr };
	QVBoxLayout	*m_vboxLayout	{ nullptr };
	QToolButton	*m_insTBtn		{ nullptr },
							*m_addTBtn		{ nullptr },
							*m_delTBtn		{ nullptr },
							*m_upTBtn			{ nullptr },
							*m_downTBtn		{ nullptr };
	QListWidget	*m_listWidget	{ nullptr };

	void insertKeyword(int index, bool doSignal = true,
										 const QString& text = QString());
	void addKeyword(bool doSignal = true, const QString& text = QString());
	bool deleteKeyword(int index = -1, bool doSignal = true);
	bool moveKeywordUp(int index, bool doSignal = true);
	bool moveKeywordDown(int index, bool doSignal = true);

	virtual bool eventFilter(QObject* watched, QEvent* event) override;

signals:
	void modified();

public slots:
};

#endif // KEYWORKDSWIDGET_HPP
