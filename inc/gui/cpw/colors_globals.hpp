#ifndef COLORS_GLOBALS_HPP
#define COLORS_GLOBALS_HPP

#include <cstddef>
#include <QString>
#include <QColor>

struct ColorProperties
{
	QString	name;
	QColor	color;
};

extern ColorProperties	g_htmlColors[];
extern size_t						g_numHtmlColors;
extern ColorProperties	g_standardColors[];
extern size_t						g_numStandardColors;
extern ColorProperties	g_tonalColors[];
extern size_t						g_numTonalColors;

#endif // COLORS_GLOBALS_HPP
