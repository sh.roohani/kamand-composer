#ifndef COLORPICKERWIDGET_HPP
#define COLORPICKERWIDGET_HPP

#include <QObject>
#include <QWidget>

#include <gui/cpw/colors_globals.hpp>

class QVBoxLayout;
class QGridLayout;
class QLabel;
class QFrame;
class QComboBox;
class ColorWidget;
class QHBoxLayout;
class QToolButton;

class ColorPickerWidget : public QWidget
{
	Q_OBJECT
public:
	enum class ColorType
	{
		Custom,
		HTML,
		Tonal,
		Standard
	};
	Q_ENUM(ColorType)

	static const int	ROWS;
	static const int	COLS;

	explicit ColorPickerWidget(QWidget *parent = nullptr);

	void setAutoColorText(const QString& text);

	const QList<ColorProperties>& customColors() const { return m_customColors; }
	void setCustomColors(const QList<ColorProperties>& colors);
	void addCustomColor(const ColorProperties& color);
	const QList<ColorProperties>& recentColors() const { return m_recentColors; }
	void setRecentColors(const QList<ColorProperties>& colors);
	void addRecentColor(const ColorProperties& color);

private:
	QVBoxLayout							*m_vboxLayout					{ nullptr };
	QGridLayout							*m_gridLayout					{ nullptr };
	QLabel									*m_titleLabel					{ nullptr },
													*m_recentLabel				{ nullptr };
	QFrame									*m_hLine0							{ nullptr },
													*m_hLine1							{ nullptr },
													*m_hLine2							{ nullptr };
	QComboBox								*m_colorTypeComboBox	{ nullptr };
	QHBoxLayout							*m_hboxLayout					{ nullptr };
	QToolButton							*m_autoColorToolBtn		{ nullptr },
													*m_customColorToolBtn	{ nullptr };
	QList<ColorProperties>	m_customColors,
													m_recentColors;

	void fillCustom();
	void fillHtml();
	void fillTonal();
	void fillStandard();
	void fillRecent();
	QString rgbColorText(QColor c);

signals:
	void colorSelected(const QColor& color);
	void automaticSelected();

private slots:
	void onColorSelected(ColorWidget* w, const QColor& color);

public slots:
};

#endif // COLORPICKERWIDGET_HPP
