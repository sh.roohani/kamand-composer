#ifndef COLORWIDGET_HPP
#define COLORWIDGET_HPP

#include <QObject>
#include <QWidget>
#include <QColor>

class ColorWidget : public QWidget
{
	Q_OBJECT
public:
	explicit ColorWidget(QWidget *parent = nullptr);
	explicit ColorWidget(const QColor& color, QWidget *parent = nullptr);

	void setColor(const QColor& color);
	const QColor color() const { return m_color; }

protected:
	virtual void paintEvent(QPaintEvent* e) override;
	virtual void enterEvent(QEvent* e) override;
	virtual void leaveEvent(QEvent *e) override;
	virtual void mousePressEvent(QMouseEvent* e) override;
	virtual void mouseReleaseEvent(QMouseEvent* e) override;

private:
	QColor	m_color;
	bool		m_enter		{ false },
					m_pressed	{ false };

signals:
	void colorSelected(ColorWidget* w, const QColor& color);

public slots:
};

#endif // COLORWIDGET_HPP
