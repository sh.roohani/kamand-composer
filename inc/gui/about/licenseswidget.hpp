#ifndef LICENSESWIDGET_HPP
#define LICENSESWIDGET_HPP

#include <QWidget>

namespace Ui {
class LicensesWidget;
}

class QLabel;

class LicensesWidget : public QWidget
{
	Q_OBJECT

public:
	explicit LicensesWidget(QWidget *parent = nullptr);
	~LicensesWidget();

private:
	Ui::LicensesWidget	*ui;
	QLabel							*m_label	{ nullptr };

signals:
	void aboutClicked();
};

#endif // LICENSESWIDGET_HPP
