#ifndef ABOUTWIDGET_HPP
#define ABOUTWIDGET_HPP

#include <QWidget>

namespace Ui {
class AboutWidget;
}

class AboutWidget : public QWidget
{
	Q_OBJECT

public:
	explicit AboutWidget(QWidget *parent = nullptr);
	~AboutWidget();

private:
	Ui::AboutWidget *ui;
};

#endif // ABOUTWIDGET_HPP
