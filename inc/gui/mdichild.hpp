#ifndef MDICHILD_HPP
#define MDICHILD_HPP

#include <QWidget>
#include <QDomDocument>
#include <QTextCharFormat>

#include "kc_global_types.hpp"

namespace Ui {
class MdiChild;
}

class QMdiSubWindow;
class EditorWidget;

class MdiChild : public QWidget
{
	Q_OBJECT

public:
	explicit MdiChild(QWidget *parent = nullptr);
	~MdiChild();

	QMdiSubWindow* mdiSubWindow() { return m_mdiSubWindow; }
	void setMdiSubWindow(QMdiSubWindow* w);
	QString title(bool includeModified = true) const;
	void updateWindowTitle();
	QList<Field> fields() const;
	QString name() const;
	Section section() const;
	bool isModified() const;
	void setClean();
	void setDocumentName(Language l, const QString& name);
	const QString documentName(Language l) const;
	void addField(Field f);
	void insertField(int index, Field f);
	void removeField(Field f);
	bool isCopyAvailable();
	void copy();
	bool isUndoAvailable();
	void undo();
	bool isRedoAvailable();
	void redo();
	bool currentFieldAcceptsFormatting();
	bool currentFieldAcceptsInsertion();

	void setBoldEnabled(bool enabled);
	void setItalicEnabled(bool enabled);
	void setUnderlineEnabled(bool enabled);
	void setStrikeThroughEnabled(bool enabled);
	void removeFormatting();
	void setTextColor(const QColor& color);
	void setAutomaticTextColor();
	void setHighLightColor(const QColor& color);
	void setAutomaticHighLightColor();
	void setLeftAligned();
	void setRightAligned();
	void setLeftToRight();
	void setRightToLeft();
	void insertTable(int hCells, int vCells);
	void insertNumberedList();
	void insertBulletedList();
	void insertHyperLink(const QString& hyperLink);
	void removeHyperLink();

	void setMultilineTextEditorsFont(Language l, const QFont& fnt);

	QDomDocument xml(Language l) const;
	QDomDocument xmlTemplate() const;
	void setXmlTemplate(const QDomDocument& doc);

private:
	Ui::MdiChild	*ui;
	QMdiSubWindow	*m_mdiSubWindow	{ nullptr };
	EditorWidget	*m_enForm				{ nullptr },
								*m_faForm				{ nullptr };

	EditorWidget* currentEditor();

signals:
	void modified(Language language, Field field);
	void currentCharFormatChanged(const QTextCharFormat& f);
	void currentFocusChanged(const QTextCharFormat& f, bool applicable);

private slots:
	void onMoveRowUpRequested(int rowNumber, EditorWidget* ew);
	void onMoveRowDownRequested(int rowNumber, EditorWidget* ew);
	void onModified(Language language, Field field);
};

#endif // MDICHILD_HPP
