#ifndef UPDOWNBUTTONSWIDGET_HPP
#define UPDOWNBUTTONSWIDGET_HPP

#include <QWidget>

class QVBoxLayout;
class QToolButton;

class UpDownButtonsWidget : public QWidget
{
	Q_OBJECT
public:
	explicit UpDownButtonsWidget(QWidget *parent = nullptr);
	explicit UpDownButtonsWidget(int rownNumber, QWidget *parent = nullptr);

	void setRownNumber(int rowNumber);
	int rownNumber() const { return m_rowNumber; }
	void setUpEnabled(bool enabled);
	bool isUpEnabled() const;
	void setDownEnabled(bool enabled);
	bool isDownEnabled() const;

private:
	QVBoxLayout	*m_vboxLayout	{ nullptr };
	QToolButton	*m_upButton		{ nullptr },
							*m_downButton	{ nullptr };
	int					m_rowNumber		{ -1 };

	void prepare();

signals:
	void upClicked(int rowNumber);
	void downClicked(int rowNumber);

public slots:
};

#endif // UPDOWNBUTTONSWIDGET_HPP
