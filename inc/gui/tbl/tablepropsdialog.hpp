#ifndef TABLEPROPSDIALOG_HPP
#define TABLEPROPSDIALOG_HPP

#include <QDialog>

namespace Ui {
class TablePropsDialog;
}

class TablePropsDialog : public QDialog
{
	Q_OBJECT

public:
	explicit TablePropsDialog(QWidget *parent = nullptr);
	~TablePropsDialog();

	int	m_hCells	{ 2 },
			m_vCells	{ 2 };

	void populateUi();
	void collectUi();

private:
	Ui::TablePropsDialog *ui;
};

#endif // TABLEPROPSDIALOG_HPP
