#ifndef TABLEWIDGET_HPP
#define TABLEWIDGET_HPP

#include <QWidget>

class TableWidget : public QWidget
{
	Q_OBJECT
public:
	explicit TableWidget(QWidget *parent = nullptr);

	virtual QSize sizeHint() const override;

protected:
	virtual void paintEvent(QPaintEvent* e) override;
	virtual void mouseMoveEvent(QMouseEvent* e) override;
	virtual void mousePressEvent(QMouseEvent* e) override;
	virtual void mouseReleaseEvent(QMouseEvent *e) override;
	virtual void leaveEvent(QEvent *e) override;

private:
	static const int	CELL_SIZE;
	static const int	HORZ_CELLS;
	static const int	VERT_CELLS;

	int		m_activeHCells	{ 0 },
				m_activeVCells	{ 0 };
	bool	m_mousePressed	{ false };

signals:
	void tableSizeSelected(int hCells, int vCells);

public slots:
};

#endif // TABLEWIDGET_HPP
