#ifndef TABLEINSERTIONDIALOG_HPP
#define TABLEINSERTIONDIALOG_HPP

#include <QDialog>

namespace Ui {
class TableInsertionDialog;
}

class TableInsertionDialog : public QDialog
{
	Q_OBJECT

public:
	explicit TableInsertionDialog(QWidget *parent = nullptr);
	~TableInsertionDialog();

	void populateUi();
	void collectUi();

	int		m_number	{ 1 };
	bool	m_before	{ true };

private:
	Ui::TableInsertionDialog *ui;
};

#endif // TABLEINSERTIONDIALOG_HPP
