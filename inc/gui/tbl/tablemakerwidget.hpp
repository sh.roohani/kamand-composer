#ifndef TABLEMAKERWIDGET_HPP
#define TABLEMAKERWIDGET_HPP

#include <QWidget>

class QVBoxLayout;
class TableWidget;
class QPushButton;
class QLabel;

class TableMakerWidget : public QWidget
{
	Q_OBJECT
public:
	explicit TableMakerWidget(QWidget *parent = nullptr);

private:
	QVBoxLayout	*m_vboxLayout		{ nullptr };
	QLabel			*m_titleLabel		{ nullptr };
	TableWidget	*m_tableWidget	{ nullptr };
	QPushButton	*m_moreOptsBtn	{ nullptr };

signals:
	void tableSizeSelected(int hCells, int vCells);

public slots:
};

#endif // TABLEMAKERWIDGET_HPP
