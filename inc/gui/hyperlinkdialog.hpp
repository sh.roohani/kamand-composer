#ifndef HYPERLINKDIALOG_HPP
#define HYPERLINKDIALOG_HPP

#include <QDialog>

namespace Ui {
class HyperLinkDialog;
}

class HyperLinkDialog : public QDialog
{
	Q_OBJECT

public:
	explicit HyperLinkDialog(QWidget *parent = nullptr);
	~HyperLinkDialog();

	void populateUi();
	void collectUi();

	QString	m_hyperlink;

private:
	Ui::HyperLinkDialog *ui;
};

#endif // HYPERLINKDIALOG_HPP
