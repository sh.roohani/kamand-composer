#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QImage>
#include <QBitmap>
#include <QPixmap>
#include <QIcon>
#include <QTextCharFormat>

#include "kc_global_types.hpp"

namespace Ui {
class MainWindow;
}

class QMdiArea;
class MdiChild;
class QToolButton;
class ColorPickerWidget;
class QActionGroup;
class TableMakerWidget;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

	void loadConfigurationParameters();
	void saveConfigurationParameters();

private:
	Ui::MainWindow		*ui;
	QMdiArea					*m_mdiArea				{ nullptr };
	QBitmap						m_colorMask,
										m_colorMaskOverlay;
	QPixmap						m_txtClrImg				{ "://img/text-color.png"},
										m_hltClrImg				{ "://img/background-color.png" };
	QToolButton				*m_fldToolBtn			{ nullptr },
										*m_txtClrToolBtn	{ nullptr },
										*m_hltClrToolBtn	{ nullptr },
										*m_tblToolBtn			{ nullptr },
										*m_lstToolBtn			{ nullptr };
	ColorPickerWidget	*m_txtClrPickWid	{ nullptr },
										*m_hltClrPickWid	{ nullptr };
	QActionGroup			*m_alignActGrp		{ nullptr },
										*m_dirActGrp			{ nullptr };
	TableMakerWidget	*m_tblMakerWid		{ nullptr };
	QColor						m_currTxtColor,
										m_currHltColor;

	void prepareEditMenuConnections();
	void prepareInsertMenuConnections();
	void prepareFileMenuConnections();
	void prepareFormatMenuConnections();
	void prepareViewMenuConnections();
	void prepareWindowMenuConnections();
	void prepareHelpMenuConnections();
	void prepareToolBarToolButtonConnections();
	void prepareActions();
	void prepareConnections();
	void prepareMenus();
	void prepareToolBars();
	MdiChild* newMdiChild();
	MdiChild* currentMdiChild();
	QList<MdiChild*> mdiChildList();
	QIcon textColorIcon(const QColor& color);
	QIcon textAutoColorIcon();
	QIcon highLightColorIcon(const QColor& color);
	QIcon highLightAutoColorIcon();
	void applyMlteFonts();
	void updateWindowTitle(const QString& postfix);
	void updateEditFieldsMenu(MdiChild* mdiChild);
	static void getDocumentSaveFileNames(QWidget* parent, MdiChild* mdiChild,
																			 const QString& caption,
																			 QString& enName, QString& faName);
	static QString getTemplateSaveFileName(QWidget* parent, const QString& caption);
	bool fileExists(const QString&  fileName);
	void saveDocument(const QString& docName, const QByteArray& doc);

private slots:
	void updateMenus();
	void updateWindowTitle();
	void updateToolBars();
	void onFileNewDocument();
	void onFileNewFromTemplate();
	void onFileOpen();
	void onFileClose();
	void onFileCloseAll();
	void onFileSave();
	void onFileSaveAsDocument();
	void onFileSaveAsTemplate();
	void onFileSaveAll();
	void onFileExit();
	void onEditUndo();
	void onEditRedo();
	void onEditCut();
	void onEditCopy();
	void onEditPaste();
	void onEditFields(Field f, bool checked);
	void onEditPreferences();
	void onInsertTable();
	void onInsertHyperlink(bool checked);
	void onInsertListBulleted();
	void onInsertListNumbered();
	void onFormatTextBold(bool checked);
	void onFormatTextItalic(bool checked);
	void onFormatTextUnderline(bool checked);
	void onFormatTextStrikeThrough(bool checked);
	void onFormatTextRemoveFormatting();
	void onFormatColorText();
	void onFormatColorHighLight();
	void onFormatParagraphLeftAligned();
	void onFormatParagraphRightAligned();
	void onFormatParagraphLeftToRight();
	void onFormatParagraphRightToLeft();
	void onViewFullScreen(bool checked);
	void onViewMenuBar(bool checked);
	void onViewToolBar(bool checked);
	void onWindowTabbedView(bool checked);
	void onWindowTile();
	void onWindowTileHorizontally();
	void onWindowTileVertically();
	void onWindowCascade();
	void onHelpAbout();
	void onHelpAboutQt();

	void applyTextColor(const QColor& color);
	void applyAutomaticTextColor();
	void applyHighLightColor(const QColor& color);
	void applyAutomaticHighLightColor();
	void insertTable(int hCells, int vCells);
	void insertNumberedList();
	void insertBulletedList();
	void insertHyperLink(const QString& hyperLink);

	void onCurrentTextFormatChanged(const QTextCharFormat& f);
	void onCurrentFocusChanged(const QTextCharFormat& f, bool applicable);
	void onMdiChildModified(MdiChild* mdiChild, Language language, Field field);
};

#endif // MAINWINDOW_HPP
