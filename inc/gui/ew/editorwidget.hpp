#ifndef EDITORWIDGET_HPP
#define EDITORWIDGET_HPP

#include <functional>

#include <QWidget>
#include <QDomDocument>
#include <QTextCharFormat>

#include "kc_global_types.hpp"

class QVBoxLayout;
class QFormLayout;
class QScrollArea;
class UpDownButtonsWidget;
class QComboBox;
class QLineEdit;
class QTextEdit;
class KeywordsWidget;
class QMenu;

class EditorWidget : public QWidget
{
	Q_OBJECT
public:
	explicit EditorWidget(Language lang, QWidget *parent = nullptr);

	void setClean();
	void setDocumentName(const QString& name);
	const QString& documentName() const { return m_docName; }
	void setName(const QString& name, bool doSignal = true);
	QString name();
	void setSection(Section section, bool doSignal = true);
	Section section();
	QList<Field> fields();
	void moveRowUp(int rowNumber);
	void moveRowDown(int rowNumber);
	int addField(Field f);
	void insertField(int index, Field f);
	void removeField(Field f);
	bool isModified() const { return m_modified; }
	bool isCopyAvailable();
	void copy();
	bool isUndoAvailable();
	void undo();
	bool isRedoAvailable();
	void redo();
	bool currentFieldAcceptsFormatting();
	bool currentFieldAcceptsInsertion();

	void setBoldEnabled(bool enabled);
	void setItalicEnabled(bool enabled);
	void setUnderlineEnabled(bool enabled);
	void setStrikeThroughEnabled(bool enabled);
	void removeFormatting();
	void setTextColor(const QColor& color);
	void setAutomaticTextColor();
	void setHighLightColor(const QColor& color);
	void setAutomaticHighLightColor();
	void setLeftAligned();
	void setRightAligned();
	void setLeftToRight();
	void setRightToLeft();
	void insertTable(int hCells, int vCells);
	void insertNumberedList();
	void insertBulletedList();
	void insertHyperLink(const QString& hyperLink);
	void removeHyperLink();

	void setMultilineTextEditorsFont(const QFont& fnt);

	QDomDocument xml();
	QDomDocument xmlTemplate();
	void setXmlTemplate(const QDomDocument& doc);

protected:
	virtual void showEvent(QShowEvent* event) override;

private:
	static const QString	PROPERTY_INIT_STATE;
	static const QString	PROPERTY_FIELD_TYPE;
	static const QString	PROPERTY_COPY_AVAILABLE;
	static const QString	PROPERTY_UNDO_AVAILABLE;
	static const QString	PROPERTY_REDO_AVAILABLE;

	QFormLayout	*m_formLayout		{ nullptr };
	QScrollArea	*m_scrollArea		{ nullptr };
	bool				m_modified			{ false };
	Language		m_lang					{ Language::English };
	QString			m_docName;
	QFont				m_mlteFont;

	void applyMlteFont();
	void setSection(int section, bool doSignal = true);
	void setModified(QWidget* widget, bool state);
	QString fieldString(Field f);
	QString sectionString(Section s);
	void addRow(int rowNumber,
							const QString& labelText,
							QWidget* field,
							Field fieldType,
							QSizePolicy::Policy horizontal = QSizePolicy::Expanding,
							QSizePolicy::Policy vertical = QSizePolicy::Fixed);
	void updateRowNumbers();
	Field rowType(int rowNumber);
	Field rowType(QWidget* widget);
	int fieldRowNumber(Field f);
	QWidget* rowWidget(int rowNumber);
	QWidget* currentWidget();
	QWidget* findField(Field f, int *rowNumber = nullptr);
	UpDownButtonsWidget* rowUpDownButtonsWidget(int rowNumber);
	void visitRowWidgets(std::function<bool(QWidget*)> visitor);

	void prepareComboBoxConnections(QComboBox* cb);
	void prepareLineEditConnections(QLineEdit* le);
	void prepareTextEditConnections(QTextEdit* te);
	void prepareKeywordsWidgetConnections(KeywordsWidget* kw);

	QString colorHexString(const QColor& c);
	QDomElement textEditElement(Field field, QDomDocument& doc);
	QDomElement keywordsElement(QDomDocument& doc);

	QMenu* textTableMenu(QTextEdit* te, const QPoint& pt);
	QMenu* textListMenu(QTextEdit* te, const QPoint& pt);
	QMenu* standardMenu(QTextEdit* te, const QPoint& pt);
	void showCustomContextMenu(QTextEdit* te, const QPoint& pt);
	virtual bool eventFilter(QObject* watched, QEvent* event) override;

signals:
	void moveRowUpRequested(int rowNumber, EditorWidget* ew);
	void moveRowDownRequested(int rowNumber, EditorWidget* ew);
	void modified(Language language, Field field);
	void currentCharFormatChanged(const QTextCharFormat& f);
	void currentFocusChanged(const QTextCharFormat& f, bool applicable);
	void copyAvailable(QWidget* widget, bool yes);
	void undoAvailable(QWidget* widget, bool yes);
	void redoAvailable(QWidget* widget, bool yes);

public slots:

private slots:
	void onUpClicked(int rowNumber);
	void onDownClicked(int rowNumber);
};

#endif // EDITORWIDGET_HPP
