#ifndef PREFS_GENERAL_WIDGET_HPP
#define PREFS_GENERAL_WIDGET_HPP

#include "gui/prefs/prefsbasewidget.hpp"

namespace Ui {
class PrefsGeneralWidget;
}

class Config;

class PrefsGeneralWidget : public PrefsBaseWidget
{
  Q_OBJECT

public:
	explicit PrefsGeneralWidget(QWidget *parent = nullptr);
	~PrefsGeneralWidget();

  virtual void populateUi(Config* cfg) override;
  virtual void collectUi(Config* cfg) override;

private:
	Ui::PrefsGeneralWidget *ui;
};

#endif // PREFS_GENERAL_WIDGET_HPP
