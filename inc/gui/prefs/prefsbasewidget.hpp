#ifndef PREFS_BASE_WIDGET_HPP
#define PREFS_BASE_WIDGET_HPP

#include <QWidget>

class Config;

class PrefsBaseWidget : public QWidget
{
  Q_OBJECT
public:
	explicit PrefsBaseWidget(QWidget *parent = nullptr);

  virtual void populateUi(Config* cfg) = 0;
  virtual void collectUi(Config* cfg) = 0;

signals:

public slots:
};

#endif // PREFS_BASE_WIDGET_HPP
