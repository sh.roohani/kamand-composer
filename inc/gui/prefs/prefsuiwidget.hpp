#ifndef PREFS_UI_WIDGET_HPP
#define PREFS_UI_WIDGET_HPP

#include "gui/prefs/prefsbasewidget.hpp"

#include <QFont>
#include <kc_global_types.hpp>

namespace Ui {
class PrefsUiWidget;
}

class Config;
class QLabel;
class QTextEdit;

class PrefsUiWidget : public PrefsBaseWidget
{
  Q_OBJECT

public:
	explicit PrefsUiWidget(QWidget *parent = nullptr);
	~PrefsUiWidget();

  virtual void populateUi(Config* cfg) override;
  virtual void collectUi(Config* cfg) override;

private:
	Ui::PrefsUiWidget	*ui;
	QFont							m_enMlteFont,
										m_faMlteFont;

	bool browseFont(QFont& f);
	void setFontInfo(const QFont& fnt, QLabel* lbl, QTextEdit* te);
	void chooseFont(Language l);
};

#endif // PREFS_UI_WIDGET_HPP
