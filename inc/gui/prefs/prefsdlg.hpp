#ifndef PREFS_DLG_HPP
#define PREFS_DLG_HPP

#include <QDialog>
#include <vector>

namespace Ui {
class PrefsDlg;
}

class Config;

class PrefsDlg : public QDialog
{
  Q_OBJECT

public:
  explicit PrefsDlg(QWidget *parent = nullptr);
  ~PrefsDlg();

  void populateUi(Config* cfg);
  void collectUi(Config* cfg);

private:
  Ui::PrefsDlg *ui;

  QString m_title;
};

#endif // PREFS_DLG_HPP
