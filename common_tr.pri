unix {
QMAKE_POST_LINK += mkdir -p $${DESTDIR}/languages
QMAKE_POST_LINK += $$escape_expand(\n\t)
QMAKE_POST_LINK += $$quote(cp $$files($$_PRO_FILE_PWD_/lng/*.qm) $${DESTDIR}/languages)
}
