#include <kc_config.hpp>

#include <QSettings>
#include <QDir>
#include <QFileInfo>
#include <QApplication>
#include <QFileInfo>
#include <QFont>

#define KC_CFG_GRP_GENERAL						"General"
#define KC_CFG_GRP_UI									"UI"
#define KC_CFG_GRP_FORMAT							"Format"

#define KC_CFG_KEY_LAST_DOC_DIR				"LastDocumentDirectory"
#define KC_CFG_KEY_LAST_TMPL_DIR			"LastTemplateDirectory"

#define KC_CFG_KEY_MW_GEOMETRY				"MainWindowGeometry"
#define KC_CFG_KEY_MW_STATE						"MainWindowState"
#define KC_CFG_KEY_FULLSCREEN					"Fullscreen"

#define KC_CFG_KEY_NAME								"Name"
#define KC_CFG_KEY_R									"R"
#define KC_CFG_KEY_G									"G"
#define KC_CFG_KEY_B									"B"
#define KC_CFG_EN_MLTE_FONT						"EnMlteFont"
#define KC_CFG_FA_MLTE_FONT						"FaMlteFont"

#define KC_CFG_KEY_TABBED_VIEW				"TabbedView"
#define KC_CFG_KEY_EXIT_CONFIRM				"AskForExitConfirmation"

#define KC_CFG_ARR_TXT_CUSTOM_COLORS	"FontCustomColors"
#define KC_CFG_ARR_HLT_CUSTOM_COLORS	"HighLightCustomColors"
#define KC_CFG_ARR_TXT_RECENT_COLORS	"FontRecentColors"
#define KC_CFG_ARR_HLT_RECENT_COLORS	"HighLightRecentColors"

Config::Config(const QString& path):
	m_path(path)
{
}

Config::Config() :
	Config("")
{
}

Config::~Config()
{
}

bool Config::load()
{
	bool						result	= true;
	QSettings				*cfg		= nullptr;
	int							size		= 0;
	ColorProperties	cp;

	if (m_path.isEmpty())
	{

		cfg	= new QSettings(QSettings::IniFormat, QSettings::UserScope,
												QApplication::organizationName(),
												QApplication::applicationName());
	}
	else
	{
		cfg	= new QSettings(m_path, QSettings::IniFormat);
	}

	cfg->beginGroup(KC_CFG_GRP_GENERAL);
	m_lastDocDir  = cfg->value(KC_CFG_KEY_LAST_DOC_DIR).toString();
	m_lastTmplDir = cfg->value(KC_CFG_KEY_LAST_TMPL_DIR).toString();
	cfg->endGroup();

	cfg->beginGroup(KC_CFG_GRP_UI);
	m_mwGeometry        = cfg->value(KC_CFG_KEY_MW_GEOMETRY).toByteArray();
	m_mwState           = cfg->value(KC_CFG_KEY_MW_STATE).toByteArray();
	m_fullscreen				= cfg->value(KC_CFG_KEY_FULLSCREEN, m_fullscreen).toBool();
	m_tabbedView			  = cfg->value(KC_CFG_KEY_TABBED_VIEW, m_tabbedView).toBool();
	m_exitConfirmation  = cfg->value(KC_CFG_KEY_EXIT_CONFIRM, m_exitConfirmation).toBool();
	m_enMlteFont				= cfg->value(KC_CFG_EN_MLTE_FONT).toString();
	if (m_enMlteFont.isEmpty())
	{
		m_enMlteFont	= QApplication::font("QTextEdit").toString();
	}
	m_faMlteFont				= cfg->value(KC_CFG_FA_MLTE_FONT).toString();
	if (m_faMlteFont.isEmpty())
	{
		m_faMlteFont	= QApplication::font("QTextEdit").toString();
	}
	if (m_faMlteFont.isEmpty())
	cfg->endGroup();

	cfg->beginGroup(KC_CFG_GRP_FORMAT);
	size	= cfg->beginReadArray(KC_CFG_ARR_TXT_CUSTOM_COLORS);
	for (int i=0; i<size; ++i)
	{
		cfg->setArrayIndex(i);
		cp.name		= cfg->value(KC_CFG_KEY_NAME).toString();
		cp.color	= QColor(cfg->value(KC_CFG_KEY_R).toInt(),
											 cfg->value(KC_CFG_KEY_G).toInt(),
											 cfg->value(KC_CFG_KEY_B).toInt());
		m_txtCustomColors << cp;
	}
	cfg->endArray();
	size	= cfg->beginReadArray(KC_CFG_ARR_HLT_CUSTOM_COLORS);
	for (int i=0; i<size; ++i)
	{
		cfg->setArrayIndex(i);
		cp.name		= cfg->value(KC_CFG_KEY_NAME).toString();
		cp.color	= QColor(cfg->value(KC_CFG_KEY_R).toInt(),
											 cfg->value(KC_CFG_KEY_G).toInt(),
											 cfg->value(KC_CFG_KEY_B).toInt());
		m_hltCustomColors << cp;
	}
	cfg->endArray();
	size	= cfg->beginReadArray(KC_CFG_ARR_TXT_RECENT_COLORS);
	for (int i=0; i<size; ++i)
	{
		cfg->setArrayIndex(i);
		cp.name		= cfg->value(KC_CFG_KEY_NAME).toString();
		cp.color	= QColor(cfg->value(KC_CFG_KEY_R).toInt(),
											 cfg->value(KC_CFG_KEY_G).toInt(),
											 cfg->value(KC_CFG_KEY_B).toInt());
		m_txtRecentColors << cp;
	}
	cfg->endArray();
	size	= cfg->beginReadArray(KC_CFG_ARR_HLT_RECENT_COLORS);
	for (int i=0; i<size; ++i)
	{
		cfg->setArrayIndex(i);
		cp.name		= cfg->value(KC_CFG_KEY_NAME).toString();
		cp.color	= QColor(cfg->value(KC_CFG_KEY_R).toInt(),
											 cfg->value(KC_CFG_KEY_G).toInt(),
											 cfg->value(KC_CFG_KEY_B).toInt());
		m_hltRecentColors << cp;
	}
	cfg->endArray();
	cfg->endGroup();

	delete cfg;

	return result;
}

bool Config::save()
{
	bool			result	= true;
	QSettings	*cfg		= nullptr;

	if (m_path.isEmpty())
	{
		cfg	= new QSettings(QSettings::IniFormat, QSettings::UserScope,
												QApplication::organizationName(),
												QApplication::applicationName());
	}
	else
	{
		cfg	= new QSettings(m_path, QSettings::IniFormat);
	}

	cfg->beginGroup(KC_CFG_GRP_GENERAL);
	cfg->setValue(KC_CFG_KEY_LAST_TMPL_DIR, m_lastTmplDir);
	cfg->setValue(KC_CFG_KEY_LAST_DOC_DIR, m_lastDocDir);
	cfg->endGroup();

	cfg->beginGroup(KC_CFG_GRP_UI);
	cfg->setValue(KC_CFG_KEY_MW_GEOMETRY, m_mwGeometry);
	cfg->setValue(KC_CFG_KEY_MW_STATE, m_mwState);
	cfg->setValue(KC_CFG_KEY_FULLSCREEN, m_fullscreen);
	cfg->setValue(KC_CFG_KEY_TABBED_VIEW, m_tabbedView);
	cfg->setValue(KC_CFG_KEY_EXIT_CONFIRM, m_exitConfirmation);
	cfg->endGroup();

	cfg->beginGroup(KC_CFG_GRP_UI);
	m_mwGeometry        = cfg->value(KC_CFG_KEY_MW_GEOMETRY).toByteArray();
	m_mwState           = cfg->value(KC_CFG_KEY_MW_STATE).toByteArray();
	m_fullscreen				= cfg->value(KC_CFG_KEY_FULLSCREEN, m_fullscreen).toBool();
	m_exitConfirmation  = cfg->value(KC_CFG_KEY_EXIT_CONFIRM, m_exitConfirmation).toBool();
	cfg->setValue(KC_CFG_EN_MLTE_FONT, m_enMlteFont);
	cfg->setValue(KC_CFG_FA_MLTE_FONT, m_faMlteFont);
	cfg->endGroup();

	cfg->beginGroup(KC_CFG_GRP_FORMAT);
	cfg->beginWriteArray(KC_CFG_ARR_TXT_CUSTOM_COLORS);
	for (int i=0; i<m_txtCustomColors.size(); ++i)
	{
		cfg->setArrayIndex(i);
		cfg->setValue(KC_CFG_KEY_NAME, m_txtCustomColors[i].name);
		cfg->setValue(KC_CFG_KEY_R, m_txtCustomColors[i].color.red());
		cfg->setValue(KC_CFG_KEY_G, m_txtCustomColors[i].color.green());
		cfg->setValue(KC_CFG_KEY_B, m_txtCustomColors[i].color.blue());
	}
	cfg->endArray();
	cfg->beginWriteArray(KC_CFG_ARR_HLT_CUSTOM_COLORS);
	for (int i=0; i<m_hltCustomColors.size(); ++i)
	{
		cfg->setArrayIndex(i);
		cfg->setValue(KC_CFG_KEY_NAME, m_hltCustomColors[i].name);
		cfg->setValue(KC_CFG_KEY_R, m_hltCustomColors[i].color.red());
		cfg->setValue(KC_CFG_KEY_G, m_hltCustomColors[i].color.green());
		cfg->setValue(KC_CFG_KEY_B, m_hltCustomColors[i].color.blue());
	}
	cfg->endArray();
	cfg->beginWriteArray(KC_CFG_ARR_TXT_RECENT_COLORS);
	for (int i=0; i<m_txtRecentColors.size(); ++i)
	{
		cfg->setArrayIndex(i);
		cfg->setValue(KC_CFG_KEY_NAME, m_txtRecentColors[i].name);
		cfg->setValue(KC_CFG_KEY_R, m_txtRecentColors[i].color.red());
		cfg->setValue(KC_CFG_KEY_G, m_txtRecentColors[i].color.green());
		cfg->setValue(KC_CFG_KEY_B, m_txtRecentColors[i].color.blue());
	}
	cfg->endArray();
	cfg->beginWriteArray(KC_CFG_ARR_HLT_RECENT_COLORS);
	for (int i=0; i<m_hltRecentColors.size(); ++i)
	{
		cfg->setArrayIndex(i);
		cfg->setValue(KC_CFG_KEY_NAME, m_hltRecentColors[i].name);
		cfg->setValue(KC_CFG_KEY_R, m_hltRecentColors[i].color.red());
		cfg->setValue(KC_CFG_KEY_G, m_hltRecentColors[i].color.green());
		cfg->setValue(KC_CFG_KEY_B, m_hltRecentColors[i].color.blue());
	}
	cfg->endArray();
	cfg->endGroup();

	delete cfg;

	return result;
}
