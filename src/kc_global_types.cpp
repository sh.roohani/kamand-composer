#include <kc_global_types.hpp>

// Postfix
Field operator++(Field& f, int)
{
	Field	temp	= f;

	f = static_cast<Field>(static_cast<int>(f) + 1);

	return temp;
}

// Prefix
Field& operator++(Field& f)
{
	return f = static_cast<Field>(static_cast<int>(f) + 1);
}

// Postfix
Section operator++(Section& s, int)
{
	Section	temp	= s;

	s = static_cast<Section>(static_cast<int>(s) + 1);

	return temp;
}

// Prefix
Section& operator++(Section& s)
{
	return s = static_cast<Section>(static_cast<int>(s) + 1);
}
