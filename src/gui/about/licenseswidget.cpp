#include <gui/about/licenseswidget.hpp>
#include "ui_licenseswidget.h"

#include <QTextStream>

#include <QDesktopServices>
#include <QUrl>
#include <QLabel>

LicensesWidget::LicensesWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::LicensesWidget)
{
	ui->setupUi(this);

	m_label	= new QLabel();

	ui->scrollArea->setWidget(m_label);
	ui->scrollArea->setStyleSheet("background-color: transparent;");

	QString			lic;
	QTextStream	ts(&lic);

	ts << "All icons (except for the application icon, which is visible on "
				"the <a href=\"#about\">About</a> tab of this dialog box), "
				"courtesy of <a href=\"https://icons8.com/\">Icons8</a>."
		 << "<br><br>"
		 << "The application icon is a work of art by "
				"<a href=\"mailto:jalaeihamed@gmail.com\">Hamed Jalaei</a>."
		 << "<br><hr><br>"
		 << "This program is free software: you can redistribute it and/or modify "
				"it under the terms of the GNU General Public License as published by "
				"the Free Software Foundation, either version 3 of the License, or "
				"(at your option) any later version.<br><br>"
				"This program is distributed in the hope that it will be useful, "
				"but WITHOUT ANY WARRANTY; without even the implied warranty of "
				"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
				"GNU General Public License for more details.<br><br>"
				"You should have received a copy of the GNU General Public License "
				"along with this program.  If not, see "
				"<a href=\"https://www.gnu.org/licenses/\">&lt;https://www.gnu.org/licenses/&gt;</a>.";

	m_label->setWordWrap(true);
	m_label->setAlignment(Qt::AlignLeft | Qt::AlignTop);
	m_label->setText(lic);
	m_label->setTextInteractionFlags(Qt::TextSelectableByMouse |
																	 Qt::TextSelectableByKeyboard |
																	 Qt::LinksAccessibleByMouse |
																	 Qt::LinksAccessibleByKeyboard);

	connect(m_label, &QLabel::linkActivated, [&](const QString& link)
	{
		if (link == "#about")
		{
			emit aboutClicked();
		}
		else
		{
			QDesktopServices::openUrl(QUrl(link));
		}
	});
}

LicensesWidget::~LicensesWidget()
{
	delete ui;
}
