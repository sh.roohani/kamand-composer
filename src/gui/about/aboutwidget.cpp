#include <gui/about/aboutwidget.hpp>
#include "ui_aboutwidget.h"

#include <kc_global_types.hpp>

#include <QPixmap>
#include <QTextStream>

AboutWidget::AboutWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::AboutWidget)
{
	ui->setupUi(this);

	QPixmap	pm(":/img/kamand.png");

	ui->iconLabel->setMinimumSize(pm.width(), pm.height());
	ui->iconLabel->setMaximumSize(pm.width(), pm.height());
	ui->iconLabel->setPixmap(pm);

	QString			desc;
	QTextStream	ts(&desc);

	ts << "<b>"
		 << tr("Kamand Composer ")
		 << KC_VER_MAJOR << "." << KC_VER_MINOR << "." << KC_VER_PATCH
		 << "</b><br><br>"
		 << "A man page translation editor for the project "
				"<a href=\"https://github.com/hamedjalaei/kamand\">Kamand</a> by "
				"<a href=\"http://www.linuxforlife.ir/\">LinuxForLife</a>.<br><br>"
				"<br>"
				"Built with <a href=\"https://www.qt.io/\">Qt</a> version "
				QT_VERSION_STR "<br>"
				"Run time Qt version is "
		 << qVersion()
		 << "<br><br><br><br>"
				"Copyright 2019 <a href=\"mailto:sh.roohani@gmail.com\">Shahram Roohani</a>"
				"<hr>"
				"The program is provided AS IS with NO WARRANTY OF ANY KIND, "
				"INCLUDING THE WARRANTY OF DESING, MERCHANTABILITY AND "
				"FITNESS FOR A PARTICULAR PURPOSE.";

	ui->descLabel->setTextInteractionFlags(Qt::TextSelectableByMouse |
																				 Qt::TextSelectableByKeyboard |
																				 Qt::LinksAccessibleByMouse |
																				 Qt::LinksAccessibleByKeyboard);

	ui->descLabel->setText(desc);
}

AboutWidget::~AboutWidget()
{
	delete ui;
}
