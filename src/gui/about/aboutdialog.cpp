#include <gui/about/aboutdialog.hpp>
#include "ui_aboutdialog.h"

#include <gui/about/aboutwidget.hpp>
#include <gui/about/licenseswidget.hpp>

AboutDialog::AboutDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::AboutDialog)
{
	ui->setupUi(this);

	while (ui->tabWidget->count())
	{
		delete ui->tabWidget->widget(0);
		ui->tabWidget->removeTab(0);
	}

	AboutWidget	*aboutWidget	= new AboutWidget();
	ui->tabWidget->addTab(aboutWidget, tr("&About"));

	LicensesWidget	*licensesWidget	= new LicensesWidget();
	ui->tabWidget->addTab(licensesWidget, tr("&Licenses"));

	connect(licensesWidget, &LicensesWidget::aboutClicked, [&]()
	{
		ui->tabWidget->setCurrentIndex(0);
	});
}

AboutDialog::~AboutDialog()
{
	delete ui;
}
