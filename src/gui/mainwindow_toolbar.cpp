#include <gui/mainwindow.hpp>
#include "ui_mainwindow.h"

#include <QWidgetAction>
#include <QToolButton>
#include <QPainter>

#include <gui/cpw/colorpickerwidget.hpp>
#include <gui/tbl/tablemakerwidget.hpp>
#include <gui/mdichild.hpp>

void MainWindow::prepareToolBars()
{
	QImage	colorMask					{ "://img/color-mask.png" },
					colorMaskOverlay	{ "://img/color-mask-overlay.png" };

	m_colorMask					= QBitmap::fromImage(colorMask.createAlphaMask());
	m_colorMaskOverlay	= QBitmap::fromImage(colorMaskOverlay.createAlphaMask());

	m_txtClrPickWid	= new ColorPickerWidget();
	m_txtClrPickWid->setWindowFlag(Qt::FramelessWindowHint);
	m_txtClrPickWid->setWindowTitle(tr("Font Color"));
	m_txtClrPickWid->setAutoColorText(tr("Automatic"));

	m_hltClrPickWid	= new ColorPickerWidget();
	m_hltClrPickWid->setWindowFlag(Qt::FramelessWindowHint);
	m_hltClrPickWid->setWindowTitle(tr("Highlight Color"));
	m_hltClrPickWid->setAutoColorText(tr("No Fill"));

	QToolButton	*btn	{ nullptr };

	btn	= new QToolButton();
	btn->addAction(ui->actionFileNewDocument);
	btn->addAction(ui->actionFileNewFromTemplate);
	btn->setPopupMode(QToolButton::MenuButtonPopup);
	btn->setDefaultAction(ui->actionFileNewDocument);
	ui->mainToolBar->addWidget(btn);

	ui->mainToolBar->addAction(ui->actionFileOpen);

	ui->mainToolBar->addSeparator();

	ui->mainToolBar->addAction(ui->actionFileSave);
	btn	= new QToolButton();
	btn->addAction(ui->actionFileSaveAsDocument);
	btn->addAction(ui->actionFileSaveAsTemplate);
	btn->setPopupMode(QToolButton::MenuButtonPopup);
	btn->setDefaultAction(ui->actionFileSaveAsDocument);
	ui->mainToolBar->addWidget(btn);

	ui->mainToolBar->addAction(ui->actionFileSaveAll);

	ui->mainToolBar->addSeparator();

	ui->mainToolBar->addAction(ui->actionEditUndo);
	ui->mainToolBar->addAction(ui->actionEditRedo);

	ui->mainToolBar->addSeparator();

	ui->mainToolBar->addAction(ui->actionEditCut);
	ui->mainToolBar->addAction(ui->actionEditCopy);
	ui->mainToolBar->addAction(ui->actionEditPaste);

	ui->mainToolBar->addSeparator();

	m_fldToolBtn	= new QToolButton();
	m_fldToolBtn->setMenu(ui->menuEditFields);
	m_fldToolBtn->setIcon(QIcon(":/img/field.png"));
	m_fldToolBtn->setPopupMode(QToolButton::MenuButtonPopup);
	ui->mainToolBar->addWidget(m_fldToolBtn);

	ui->mainToolBar->addSeparator();

	QWidgetAction	*wa		{ };
	QMenu					*menu	{ };

	m_tblToolBtn	= new QToolButton();
	wa	= new QWidgetAction(m_tblToolBtn);
	m_tblMakerWid	= new TableMakerWidget();
	wa->setDefaultWidget(m_tblMakerWid);
	menu	= new QMenu();
	menu->addAction(wa);
	m_tblToolBtn->setMenu(menu);
	m_tblToolBtn->setIcon(QIcon(":/img/insert-table.png"));
	m_tblToolBtn->setPopupMode(QToolButton::MenuButtonPopup);
	ui->mainToolBar->addWidget(m_tblToolBtn);

	ui->mainToolBar->addAction(ui->actionInsertHyperlink);

	m_lstToolBtn	= new QToolButton();
	m_lstToolBtn->setMenu(ui->menuInsertList);
	m_lstToolBtn->setIcon(QIcon(":/img/list.png"));
	m_lstToolBtn->setPopupMode(QToolButton::MenuButtonPopup);
	ui->mainToolBar->addWidget(m_lstToolBtn);

	ui->mainToolBar->addSeparator();

	ui->mainToolBar->addAction(ui->actionFormatTextBold);
	ui->mainToolBar->addAction(ui->actionFormatTextItalic);
	ui->mainToolBar->addAction(ui->actionFormatTextUnderline);
	ui->mainToolBar->addAction(ui->actionFormatTextStrikethough);

	m_txtClrToolBtn	= new QToolButton();
	wa	= new QWidgetAction(m_txtClrToolBtn);
	wa->setDefaultWidget(m_txtClrPickWid);
	menu	= new QMenu();
	menu->addAction(wa);
	m_txtClrToolBtn->setMenu(menu);
	m_txtClrToolBtn->setIcon(textColorIcon(m_currTxtColor));
	m_txtClrToolBtn->setPopupMode(QToolButton::MenuButtonPopup);
	ui->mainToolBar->addWidget(m_txtClrToolBtn);

	m_hltClrToolBtn	= new QToolButton();
	wa	= new QWidgetAction(m_hltClrToolBtn);
	wa->setDefaultWidget(m_hltClrPickWid);
	menu	= new QMenu();
	menu->addAction(wa);
	m_hltClrToolBtn->setMenu(menu);
	m_hltClrToolBtn->addAction(wa);
	m_hltClrToolBtn->setIcon(highLightColorIcon(m_currHltColor));
	m_hltClrToolBtn->setPopupMode(QToolButton::MenuButtonPopup);
	ui->mainToolBar->addWidget(m_hltClrToolBtn);

	ui->mainToolBar->addSeparator();

	ui->mainToolBar->addActions(m_alignActGrp->actions());

	ui->mainToolBar->addSeparator();

	ui->mainToolBar->addActions(m_dirActGrp->actions());
}

QIcon MainWindow::textColorIcon(const QColor &color)
{
	QPainter	p(&m_txtClrImg);

	p.setClipRegion(QRegion(m_colorMask));
	p.fillRect(m_txtClrImg.rect(), color);

	return QIcon(m_txtClrImg);
}

QIcon MainWindow::textAutoColorIcon()
{
	QPainter	p(&m_txtClrImg);

	p.setClipRegion(QRegion(m_colorMask));
	p.fillRect(m_txtClrImg.rect(), Qt::black);
	p.setClipRegion(QRegion(m_colorMaskOverlay));
	p.fillRect(m_txtClrImg.rect(), Qt::white);

	return QIcon(m_txtClrImg);
}

QIcon MainWindow::highLightColorIcon(const QColor &color)
{
	QPainter	p(&m_hltClrImg);

	p.setClipRegion(QRegion(m_colorMask));
	p.fillRect(m_hltClrImg.rect(), color);

	return QIcon(m_hltClrImg);
}

QIcon MainWindow::highLightAutoColorIcon()
{
	QPainter	p(&m_hltClrImg);

	p.setClipRegion(QRegion(m_colorMask));
	p.fillRect(m_hltClrImg.rect(), Qt::black);
	p.setClipRegion(QRegion(m_colorMaskOverlay));
	p.fillRect(m_hltClrImg.rect(), Qt::white);

	return QIcon(m_hltClrImg);
}

void MainWindow::updateToolBars()
{
	MdiChild	*mdiChild	{ currentMdiChild() };
	bool			format		{ mdiChild && mdiChild->currentFieldAcceptsFormatting() },
						insert		{ mdiChild && mdiChild->currentFieldAcceptsInsertion() };

	m_txtClrToolBtn->setEnabled(format);
	m_hltClrToolBtn->setEnabled(format);

	m_tblToolBtn->setEnabled(insert);
}
