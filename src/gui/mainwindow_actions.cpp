#include <gui/mainwindow.hpp>
#include "ui_mainwindow.h"

void MainWindow::prepareActions()
{
	addAction(ui->actionFileNewDocument);
	addAction(ui->actionFileNewFromTemplate);
	addAction(ui->actionFileOpen);
	addAction(ui->actionFileClose);
	addAction(ui->actionFileCloseAll);
	addAction(ui->actionFileSave);
	addAction(ui->actionFileSaveAsDocument);
	addAction(ui->actionFileSaveAsTemplate);
	addAction(ui->actionFileSaveAll);
	addAction(ui->actionFileExit);

	addAction(ui->actionEditUndo);
	addAction(ui->actionEditRedo);
	addAction(ui->actionEditCut);
	addAction(ui->actionEditCopy);
	addAction(ui->actionEditPaste);

	addAction(ui->actionFormatTextBold);
	addAction(ui->actionFormatTextItalic);
	addAction(ui->actionFormatTextUnderline);
	addAction(ui->actionFormatTextStrikethough);
	addAction(ui->actionFormatTextRemoveFormatting);
	addAction(ui->actionFormatParagraphLeftAligned);
	addAction(ui->actionFormatParagraphRightAligned);
	addAction(ui->actionFormatParagraphLeftToRight);
	addAction(ui->actionFormatParagraphRightToLeft);

	addAction(ui->actionViewFullScreen);
	addAction(ui->actionViewMenuBar);
	addAction(ui->actionViewToolBar);
}
