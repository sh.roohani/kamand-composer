#include <gui/keywordswidget.hpp>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListWidget>
#include <QToolButton>
#include <QIcon>
#include <QSpacerItem>
#include <QKeyEvent>

#include <QDebug>

KeywordsWidget::KeywordsWidget(QWidget *parent) : QWidget(parent)
{
	m_vboxLayout	= new QVBoxLayout();
	setLayout(m_vboxLayout);

	m_hboxLayout	= new QHBoxLayout();
	m_vboxLayout->addLayout(m_hboxLayout);

	m_insTBtn			= new QToolButton();
	m_insTBtn->setIcon(QIcon("://img/insert-row.png"));
	m_insTBtn->setToolTip(tr("<p style='white-space:pre'>Insert Keyword (Insert)</p>"));
	m_insTBtn->setFocusPolicy(Qt::NoFocus);
	m_hboxLayout->addWidget(m_insTBtn);
	connect(m_insTBtn, &QToolButton::clicked, [&](bool)
	{
		insertKeyword(m_listWidget->currentRow());
	});

	m_addTBtn			= new QToolButton();
	m_addTBtn->setIcon(QIcon("://img/add-row.png"));
	m_addTBtn->setToolTip(tr("<p style='white-space:pre'>Add Keyword (Ctrl++)</p>"));
	m_addTBtn->setFocusPolicy(Qt::NoFocus);
	m_hboxLayout->addWidget(m_addTBtn);
	connect(m_addTBtn, &QToolButton::clicked, [&](bool)
	{
		addKeyword();
	});

	m_delTBtn			= new QToolButton();
	m_delTBtn->setIcon(QIcon("://img/delete-row.png"));
	m_delTBtn->setToolTip(tr("<p style='white-space:pre'>Delete Keyword (Delete)</p>"));
	m_delTBtn->setFocusPolicy(Qt::NoFocus);
	m_hboxLayout->addWidget(m_delTBtn);
	connect(m_delTBtn, &QToolButton::clicked, [&](bool)
	{
		deleteKeyword(m_listWidget->currentRow());
	});

	QSpacerItem	*hSpacer	= new QSpacerItem(1, 1, QSizePolicy::Expanding, QSizePolicy::Fixed);
	m_hboxLayout->addItem(hSpacer);

	m_upTBtn			= new QToolButton();
	m_upTBtn->setIcon(QIcon("://img/slide-up.png"));
	m_upTBtn->setToolTip(tr("<p style='white-space:pre'>Move Keyword Up (Ctrl+UpArrow)</p>"));
	m_upTBtn->setFocusPolicy(Qt::NoFocus);
	m_hboxLayout->addWidget(m_upTBtn);
	connect(m_upTBtn, &QToolButton::clicked, [&](bool)
	{
		moveKeywordUp(m_listWidget->currentRow());
	});

	m_downTBtn		= new QToolButton();
	m_downTBtn->setIcon(QIcon("://img/slide-down.png"));
	m_downTBtn->setToolTip(tr("<p style='white-space:pre'>Move Keyword Down (Ctrl+DownArrow)</p>"));
	m_downTBtn->setFocusPolicy(Qt::NoFocus);
	m_hboxLayout->addWidget(m_downTBtn);
	connect(m_downTBtn, &QToolButton::clicked, [&](bool)
	{
		moveKeywordDown(m_listWidget->currentRow());
	});

	m_listWidget	= new QListWidget();
	m_listWidget->setDragEnabled(true);
	m_listWidget->setDragDropMode(QAbstractItemView::InternalMove);
	m_listWidget->setEditTriggers(QListWidget::DoubleClicked |
																QListWidget::EditKeyPressed);
	m_listWidget->installEventFilter(this);
	m_vboxLayout->addWidget(m_listWidget);
	setFocusPolicy(Qt::StrongFocus);
	setFocusProxy(m_listWidget);
	// TODO: Handle item drag & drop to signal modified.
	// TODO: Handle item editions to signal modified
}

QStringList KeywordsWidget::keywords()
{
	QStringList	kwList;

	for (int i=0; i<m_listWidget->count(); ++i)
	{
		kwList << m_listWidget->item(i)->text();
	}

	return kwList;
}

void KeywordsWidget::setKeywords(const QStringList& kwList, bool doSignal)
{
	for (const QString& kw: kwList)
	{
		addKeyword(false, kw);
	}

	if (doSignal)
	{
		emit modified();
	}
}

void KeywordsWidget::insertKeyword(int index, bool doSignal, const QString& text)
{
	QListWidgetItem	*item			= new QListWidgetItem();

	item->setFlags(item->flags() | Qt::ItemIsEditable | Qt::ItemIsDragEnabled);
	item->setText(text);

	if (index < 0)
	{
		index	= m_listWidget->count();
	}

	m_listWidget->insertItem(index, item);
	m_listWidget->setCurrentItem(item);
	m_listWidget->editItem(item);

	if (doSignal)
	{
		emit modified();
	}
}

void KeywordsWidget::addKeyword(bool doSignal, const QString& text)
{
	QListWidgetItem	*item			= new QListWidgetItem();

	item->setFlags(item->flags() | Qt::ItemIsEditable | Qt::ItemIsDragEnabled);
	item->setText(text);

	m_listWidget->addItem(item);
	m_listWidget->setCurrentItem(item);
	m_listWidget->editItem(item);

	if (doSignal)
	{
		emit modified();
	}
}

bool KeywordsWidget::deleteKeyword(int index, bool doSignal)
{
	if (index < 0)
	{
		index	= m_listWidget->currentRow();
	}

	if (index > -1)
	{
		delete m_listWidget->takeItem(index);

		if (doSignal)
		{
			emit modified();
		}

		return true;
	}

	return false;
}

bool KeywordsWidget::moveKeywordUp(int index, bool doSignal)
{
	if (index > 0 && index < m_listWidget->count())
	{
		QListWidgetItem	*item	= m_listWidget->takeItem(index);

		m_listWidget->insertItem(index - 1, item);
		m_listWidget->setCurrentItem(item);

		if (doSignal)
		{
			emit modified();
		}

		return true;
	}

	return false;
}

bool KeywordsWidget::moveKeywordDown(int index, bool doSignal)
{
	if (index > -1 && index < m_listWidget->count() - 1)
	{
		QListWidgetItem	*item	= m_listWidget->takeItem(index + 1);

		m_listWidget->insertItem(index, item);
		m_listWidget->setCurrentRow(index + 1);

		if (doSignal)
		{
			emit modified();
		}

		return true;
	}

	return false;
}

bool KeywordsWidget::eventFilter(QObject* watched, QEvent* event)
{
	switch (event->type())
	{
	case QEvent::KeyPress:
	{
		QKeyEvent	*e	= dynamic_cast<QKeyEvent*>(event);

		if ((e->modifiers() & Qt::ControlModifier))
		{
			switch (e->key())
			{
			case Qt::Key_Plus:
				addKeyword(m_listWidget->currentRow());

				return true;

			case Qt::Key_Up:
				moveKeywordUp(m_listWidget->currentRow());

				return true;

			case Qt::Key_Down:
				moveKeywordDown(m_listWidget->currentRow());

				return true;

			default:

				break;
			}
		}
		else
		{
			switch (e->key())
			{
			case Qt::Key_Insert:
				insertKeyword(m_listWidget->currentRow());

				return true;

			case Qt::Key_Delete:
				deleteKeyword(m_listWidget->currentRow());

				return true;

			default:

				break;
			}
		}
	}

		break;

	default:

		break;
	}

	return QWidget::eventFilter(watched, event);
}
