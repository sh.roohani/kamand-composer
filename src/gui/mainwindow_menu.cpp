#include <gui/mainwindow.hpp>
#include "ui_mainwindow.h"

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDomDocument>
#include <QColorDialog>
#include <QToolButton>
#include <QMessageBox>
#include <QActionGroup>

#include <gui/mdichild.hpp>
#include <gui/prefs/prefsdlg.hpp>
#include <gui/about/aboutdialog.hpp>
#include <kc_global_vars.hpp>
#include <kc_global_types.hpp>
#include <gui/tbl/tablepropsdialog.hpp>
#include <gui/hyperlinkdialog.hpp>

void MainWindow::prepareMenus()
{
	m_alignActGrp	= new QActionGroup(this);
	m_alignActGrp->addAction(ui->actionFormatParagraphLeftAligned);
	m_alignActGrp->addAction(ui->actionFormatParagraphRightAligned);
	m_alignActGrp->setExclusive(true);

	m_dirActGrp	= new QActionGroup(this);
	m_dirActGrp->addAction(ui->actionFormatParagraphLeftToRight);
	m_dirActGrp->addAction(ui->actionFormatParagraphRightToLeft);
	m_dirActGrp->setExclusive(true);
}

void MainWindow::updateMenus()
{
	MdiChild					*mdiChild		{ currentMdiChild() };
	QList<MdiChild*>	mdiChildren	{ mdiChildList() };
	bool							anyModified	{ false },
										format			{ mdiChild && mdiChild->currentFieldAcceptsFormatting() },
										insert			{ mdiChild && mdiChild->currentFieldAcceptsInsertion() };

	for (const auto c: mdiChildren)
	{
		anyModified	|= c->isModified();
	}

	ui->actionFileClose->setEnabled(mdiChild);
	ui->actionFileCloseAll->setEnabled(mdiChild);
	ui->actionFileSave->setEnabled(mdiChild && mdiChild->isModified());
	ui->actionFileSaveAsDocument->setEnabled(mdiChild);
	ui->actionFileSaveAsTemplate->setEnabled(mdiChild);
	ui->actionFileSaveAll->setEnabled(anyModified);

	ui->actionEditUndo->setEnabled(mdiChild);
	ui->actionEditRedo->setEnabled(mdiChild);
	ui->actionEditCut->setEnabled(mdiChild);
	ui->actionEditCopy->setEnabled(mdiChild);
	ui->actionEditPaste->setEnabled(mdiChild);
	updateEditFieldsMenu(mdiChild);

	ui->actionInsertTable->setEnabled(insert);
	ui->actionInsertHyperlink->setEnabled(insert);
	ui->actionInsertListBulleted->setEnabled(insert);
	ui->actionInsertListNumbered->setEnabled(insert);

	ui->actionFormatTextBold->setEnabled(format);
	ui->actionFormatTextItalic->setEnabled(format);
	ui->actionFormatTextUnderline->setEnabled(format);
	ui->actionFormatTextStrikethough->setEnabled(format);
	ui->actionFormatTextRemoveFormatting->setEnabled(format);
	ui->actionFormatColorHighlight->setEnabled(format);
	ui->actionFormatColorHighlight->setEnabled(format);
	ui->actionFormatParagraphLeftAligned->setEnabled(format);
	ui->actionFormatParagraphRightAligned->setEnabled(format);
	ui->actionFormatParagraphLeftToRight->setEnabled(format);
	ui->actionFormatParagraphRightToLeft->setEnabled(format);
}

void MainWindow::onFileNewDocument()
{
	MdiChild			*mdiChild	{ newMdiChild() };
	QFile					f					{ ":/xml/default_template.kct" };
	QDomDocument	doc;
	QFont					fnt;

	f.open(QIODevice::ReadOnly);

	doc.setContent(f.readAll());

	f.close();

	fnt.fromString(g_cfg->m_enMlteFont);
	mdiChild->setMultilineTextEditorsFont(Language::English, fnt);
	fnt.fromString(g_cfg->m_faMlteFont);
	mdiChild->setMultilineTextEditorsFont(Language::Persian, fnt);

	mdiChild->setXmlTemplate(doc);
	mdiChild->show();
}

void MainWindow::onFileNewFromTemplate()
{
	QString	filters	{ KC_APP_NAME " Document Templates (*" KC_EXT_TEMPLATE ");;All Files (*.*)" },
					fName		{ QFileDialog::getOpenFileName(this, "New From Template",
																								 g_cfg->m_lastTmplDir,
																								 filters) };
	if (!fName.isEmpty())
	{
		MdiChild			*mdiChild	{ newMdiChild() };
		QFile					f					{ fName };
		QDomDocument	doc;
		QDir					dir;

		f.open(QIODevice::ReadOnly);

		doc.setContent(f.readAll());

		f.close();

		mdiChild->setXmlTemplate(doc);
		mdiChild->show();

		g_cfg->m_lastTmplDir	= dir.filePath(fName);
	}
}

void MainWindow::onFileOpen()
{

}

void MainWindow::onFileClose()
{
	qDebug() << "Close";
}

void MainWindow::onFileCloseAll()
{
	qDebug() << "Close All";
}

void MainWindow::onFileSave()
{
	MdiChild	*mdiChild	{ currentMdiChild() };

	if (mdiChild)
	{
		if (mdiChild->name().isEmpty())
		{
			QMessageBox::critical(this, tr("Error"),
														tr("Please assign a name to this document "
															 "to be able to save it."));

			return;
		}

		QString	enDocName		{ mdiChild->documentName(Language::English) },
						faDocName		{ mdiChild->documentName(Language::Persian) };
		bool		hasDocName	{ !enDocName.isEmpty() && !faDocName.isEmpty()};

		if (!hasDocName)
		{
			getDocumentSaveFileNames(this, mdiChild, "Save Document", enDocName, faDocName);
		}

		if (enDocName.isEmpty() || faDocName.isEmpty())
		{
			return;
		}

		if (!hasDocName && (fileExists(enDocName) || fileExists(faDocName)))
		{
			if (QMessageBox::question(this, tr("Confirm"),
																tr("At least one of the files you are going to "
																	 "save already exists.\n Replace?"),
																QMessageBox::Yes | QMessageBox::No,
																QMessageBox::No) == QMessageBox::No)
			{
				return;
			}
		}

		QDir	dir;

		saveDocument(enDocName, mdiChild->xml(Language::English).toString(2).toUtf8());
		mdiChild->setDocumentName(Language::English, enDocName);
		saveDocument(faDocName, mdiChild->xml(Language::Persian).toString(2).toUtf8());
		mdiChild->setDocumentName(Language::Persian, faDocName);
		mdiChild->setClean();
		updateWindowTitle();
		updateMenus();
		g_cfg->m_lastDocDir	= dir.filePath(enDocName);
	}
}

void MainWindow::onFileSaveAsDocument()
{
	MdiChild	*mdiChild	{ currentMdiChild() };

	if (mdiChild)
	{
		if (mdiChild->name().isEmpty())
		{
			QMessageBox::critical(this, tr("Error"),
														tr("Please assign a name to this document "
															 "to be able to save it."));

			return;
		}

		QString	enDocName,
						faDocName;

		getDocumentSaveFileNames(this, mdiChild, "Save Document As", enDocName, faDocName);

		if (enDocName.isEmpty() || faDocName.isEmpty())
		{
			return;
		}

		if (fileExists(enDocName) || fileExists(faDocName))
		{
			if (QMessageBox::question(this, tr("Confirm"),
																tr("At least one of the files you are going to "
																	 "save already exists.\n Replace?"),
																QMessageBox::Yes | QMessageBox::No,
																QMessageBox::No) == QMessageBox::No)
			{
				return;
			}
		}

		QDir	dir;

		saveDocument(enDocName, mdiChild->xml(Language::English).toString(2).toUtf8());
		mdiChild->setDocumentName(Language::English, enDocName);
		saveDocument(faDocName, mdiChild->xml(Language::Persian).toString(2).toUtf8());
		mdiChild->setDocumentName(Language::Persian, faDocName);
		mdiChild->setClean();
		updateWindowTitle();
		updateMenus();
		g_cfg->m_lastDocDir	= dir.filePath(enDocName);
	}
}

void MainWindow::onFileSaveAsTemplate()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		QString	fName	{ getTemplateSaveFileName(this, "Save Template") };

		if (!fName.isEmpty())
		{
			if (!fName.endsWith(KC_EXT_TEMPLATE, Qt::CaseInsensitive))
			{
				fName.append(KC_EXT_TEMPLATE);
			}

			QFile					f		{ fName };
			QDomDocument	doc	{ mdiChild->xmlTemplate() };
			QDir					dir;

			f.open(QIODevice::WriteOnly);

			f.write(doc.toString(2).toUtf8());

			g_cfg->m_lastTmplDir	= dir.filePath(fName);
		}
	}
}

void MainWindow::onFileSaveAll()
{

}

void MainWindow::onFileExit()
{
	qApp->quit();
}

void MainWindow::onEditUndo()
{

}

void MainWindow::onEditRedo()
{

}

void MainWindow::onEditCut()
{

}

void MainWindow::onEditCopy()
{

}

void MainWindow::onEditPaste()
{

}

void MainWindow::onEditFields(Field f, bool checked)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		checked ? mdiChild->addField(f) :
							mdiChild->removeField(f);
	}
}

void MainWindow::onEditPreferences()
{
	PrefsDlg	dlg;

	dlg.populateUi(g_cfg);

	if (dlg.exec() == QDialog::Accepted)
	{
		dlg.collectUi(g_cfg);

		applyMlteFonts();
	}
}

void MainWindow::onInsertTable()
{
	TablePropsDialog	dlg;

	if (dlg.exec() == QDialog::Accepted)
	{
		dlg.collectUi();

		insertTable(dlg.m_hCells, dlg.m_vCells);
	}
}

void MainWindow::onInsertHyperlink(bool checked)
{
	ui->actionInsertHyperlink->setIcon(checked ?
																			 QIcon(":/img/hyperlink-remove.png") :
																			 QIcon(":/img/hyperlink.png"));

	ui->actionInsertHyperlink->setToolTip(checked ?
																					tr("Remove Hyperlink") :
																					tr("Insert Hyperlink"));

	if (checked)
	{
		HyperLinkDialog	dlg;

		if (dlg.exec() == QDialog::Accepted)
		{
			dlg.collectUi();

			insertHyperLink(dlg.m_hyperlink);
		}
	}
}

void MainWindow::onInsertListBulleted()
{
	insertBulletedList();
}

void MainWindow::onInsertListNumbered()
{
	insertNumberedList();
}

void MainWindow::onFormatTextBold(bool checked)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setBoldEnabled(checked);
	}
}

void MainWindow::onFormatTextItalic(bool checked)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setItalicEnabled(checked);
	}
}

void MainWindow::onFormatTextUnderline(bool checked)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setUnderlineEnabled(checked);
	}
}

void MainWindow::onFormatTextStrikeThrough(bool checked)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setStrikeThroughEnabled(checked);
	}
}

void MainWindow::onFormatTextRemoveFormatting()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->removeFormatting();
	}
}

void MainWindow::onFormatColorText()
{
	QColorDialog	dlg;

	if (dlg.exec() == QDialog::Accepted)
	{
		m_currTxtColor	= dlg.selectedColor();

		m_txtClrToolBtn->setIcon(textColorIcon(m_currTxtColor));
		applyTextColor(m_currTxtColor);
	}
}

void MainWindow::onFormatColorHighLight()
{
	QColorDialog	dlg;

	if (dlg.exec() == QDialog::Accepted)
	{
		m_currHltColor	= dlg.selectedColor();

		m_hltClrToolBtn->setIcon(highLightColorIcon(m_currHltColor));
		applyHighLightColor(m_currHltColor);
	}
}

void MainWindow::onFormatParagraphLeftAligned()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setLeftAligned();
	}
}

void MainWindow::onFormatParagraphRightAligned()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setRightAligned();
	}
}

void MainWindow::onFormatParagraphLeftToRight()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setLeftToRight();
	}
}

void MainWindow::onFormatParagraphRightToLeft()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setRightToLeft();
	}
}

void MainWindow::onViewFullScreen(bool checked)
{
	checked ? showFullScreen() : showNormal();
}

void MainWindow::onViewMenuBar(bool checked)
{
	ui->menuBar->setVisible(checked);
}

void MainWindow::onViewToolBar(bool checked)
{
	ui->mainToolBar->setVisible(checked);
}

void MainWindow::onWindowTabbedView(bool checked)
{
	m_mdiArea->setViewMode(checked ? QMdiArea::TabbedView : QMdiArea::SubWindowView);
}

void MainWindow::onWindowTile()
{
	if (m_mdiArea->viewMode() == QMdiArea::TabbedView)
	{
		m_mdiArea->setViewMode(QMdiArea::SubWindowView);
		ui->actionWindowTabbedView->setChecked((g_cfg->m_tabbedView = false));
	}

	m_mdiArea->tileSubWindows();
}

void MainWindow::onWindowTileHorizontally()
{
	QPoint								pos						{ 0, 0 };
	QList<QMdiSubWindow*>	mdiSubWindows	{ m_mdiArea->subWindowList() };

	if (mdiSubWindows.isEmpty())
	{
		return;
	}

	for (QMdiSubWindow *w: mdiSubWindows)
	{
		QRect	rc(0, 0, m_mdiArea->width() / mdiSubWindows.size(),
						 m_mdiArea->height());

		w->setGeometry(rc);
		w->move(pos);
		pos.setX(pos.x() + w->width());
	}
}

void MainWindow::onWindowTileVertically()
{
	QPoint								pos						{ 0, 0 };
	QList<QMdiSubWindow*>	mdiSubWindows	{ m_mdiArea->subWindowList() };

	if (mdiSubWindows.isEmpty())
	{
		return;
	}

	for (QMdiSubWindow *w: mdiSubWindows)
	{
		QRect	rc(0, 0, m_mdiArea->width(),
						 m_mdiArea->height() / mdiSubWindows.size());

		w->setGeometry(rc);
		w->move(pos);
		pos.setY(pos.y() + w->height());
	}
}

void MainWindow::onWindowCascade()
{
	if (m_mdiArea->viewMode() == QMdiArea::TabbedView)
	{
		m_mdiArea->setViewMode(QMdiArea::SubWindowView);
		ui->actionWindowTabbedView->setChecked((g_cfg->m_tabbedView = false));
	}

	m_mdiArea->cascadeSubWindows();
}


void MainWindow::onHelpAbout()
{
	AboutDialog	dlg;

	dlg.exec();
}

void MainWindow::onHelpAboutQt()
{
	QMessageBox::aboutQt(this, tr("About Qt"));
}

void MainWindow::applyTextColor(const QColor& color)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setTextColor(color);
	}
}

void MainWindow::applyAutomaticTextColor()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setAutomaticTextColor();
	}
}

void MainWindow::applyHighLightColor(const QColor& color)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setHighLightColor(color);
	}
}

void MainWindow::applyAutomaticHighLightColor()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->setAutomaticHighLightColor();
	}
}

void MainWindow::insertTable(int hCells, int vCells)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->insertTable(hCells, vCells);
	}
}

void MainWindow::insertNumberedList()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->insertNumberedList();
	}
}

void MainWindow::insertBulletedList()
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->insertBulletedList();
	}
}

void MainWindow::insertHyperLink(const QString& hyperLink)
{
	MdiChild	*mdiChild	= currentMdiChild();

	if (mdiChild)
	{
		mdiChild->insertHyperLink(hyperLink);
	}
}

void MainWindow::onCurrentTextFormatChanged(const QTextCharFormat &f)
{
	QFont	font	= f.font();
	ui->actionFormatTextBold->setChecked(font.weight() == QFont::Bold);
	ui->actionFormatTextItalic->setChecked(font.italic());
	ui->actionFormatTextUnderline->setChecked(font.underline());
	ui->actionFormatTextStrikethough->setChecked(font.strikeOut());
}


void MainWindow::onCurrentFocusChanged(const QTextCharFormat& f, bool applicable)
{
	ui->actionFormatTextBold->setEnabled(applicable);
	ui->actionFormatTextItalic->setEnabled(applicable);
	ui->actionFormatTextUnderline->setEnabled(applicable);
	ui->actionFormatTextStrikethough->setEnabled(applicable);
	ui->actionFormatColorText->setEnabled(applicable);
	ui->actionFormatColorHighlight->setEnabled(applicable);

	m_txtClrToolBtn->setEnabled(applicable);
	m_hltClrToolBtn->setEnabled(applicable);

	if (applicable)
	{
		QFont	font	= f.font();

		ui->actionFormatTextBold->setChecked(font.weight() == QFont::Bold);
		ui->actionFormatTextItalic->setChecked(font.italic());
		ui->actionFormatTextUnderline->setChecked(font.underline());
		ui->actionFormatTextStrikethough->setChecked(font.strikeOut());
	}

	updateMenus();
	updateToolBars();
}

void MainWindow::onMdiChildModified(MdiChild* mdiChild,
																		Language, Field)
{
	mdiChild->updateWindowTitle();
	updateWindowTitle(mdiChild->title());
	updateMenus();
}
