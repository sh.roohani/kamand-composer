#include <gui/mdichild.hpp>
#include "ui_mdichild.h"

#include <gui/ew/editorwidget.hpp>

#include <QMdiSubWindow>

MdiChild::MdiChild(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MdiChild)
{
	ui->setupUi(this);

	while (ui->tabWidget->count())
	{
		ui->tabWidget->removeTab(0);
	}

	m_enForm	= new EditorWidget(Language::English);
	connect(m_enForm, &EditorWidget::moveRowUpRequested,
					this, &MdiChild::onMoveRowUpRequested);
	connect(m_enForm, &EditorWidget::moveRowDownRequested,
					this, &MdiChild::onMoveRowDownRequested);
	connect(m_enForm, &EditorWidget::modified,
					this, &MdiChild::onModified);
	connect(m_enForm, &EditorWidget::modified,
					this, &MdiChild::modified);
	connect(m_enForm, &EditorWidget::currentCharFormatChanged,
					this, &MdiChild::currentCharFormatChanged);
	connect(m_enForm, &EditorWidget::currentFocusChanged,
					this, &MdiChild::currentFocusChanged);

	m_faForm	= new EditorWidget(Language::Persian);
	connect(m_faForm, &EditorWidget::moveRowUpRequested,
					this, &MdiChild::onMoveRowUpRequested);
	connect(m_faForm, &EditorWidget::moveRowDownRequested,
					this, &MdiChild::onMoveRowDownRequested);
	connect(m_faForm, &EditorWidget::modified,
					this, &MdiChild::onModified);
	connect(m_faForm, &EditorWidget::modified,
					this, &MdiChild::modified);
	connect(m_faForm, &EditorWidget::currentCharFormatChanged,
					this, &MdiChild::currentCharFormatChanged);
	connect(m_faForm, &EditorWidget::currentFocusChanged,
					this, &MdiChild::currentFocusChanged);

	ui->tabWidget->addTab(m_enForm, tr("En&glish"));
	ui->tabWidget->addTab(m_faForm, tr("&Persian"));
	m_faForm->setLayoutDirection(Qt::RightToLeft);
}

MdiChild::~MdiChild()
{
	delete ui;
}

void MdiChild::setMdiSubWindow(QMdiSubWindow* w)
{
	m_mdiSubWindow	= w;
}

QString MdiChild::title(bool includeModified) const
{
	QString	title,
					modified	{ isModified() ? " *" : "" };

	title	+= name();
	title	+= QString("(%1)%2")
					 .arg(static_cast<int>(section()))
					 .arg(includeModified ? modified : "");

	return title;
}

void MdiChild::updateWindowTitle()
{
	m_mdiSubWindow->setWindowTitle(title());
}

QList<Field> MdiChild::fields() const
{
	return m_enForm->fields();
}

QString MdiChild::name() const
{
	return m_enForm->name();
}

Section MdiChild::section() const
{
	return m_enForm->section();
}

bool MdiChild::isModified() const
{
	return m_enForm->isModified() || m_faForm->isModified();
}

void MdiChild::setClean()
{
	m_enForm->setClean();
	m_faForm->setClean();

	updateWindowTitle();
}

void MdiChild::setDocumentName(Language l, const QString& name)
{
	switch (l)
	{
	case Language::English:
		m_enForm->setDocumentName(name);

		break;

	case Language::Persian:
		m_faForm->setDocumentName(name);

		break;

	default:

		break;
	}
}

const QString MdiChild::documentName(Language l) const
{
	switch (l)
	{
	case Language::English:
		return m_enForm->documentName();

	case Language::Persian:
		return m_faForm->documentName();

	default:
		return QString();
	}
}

void MdiChild::addField(Field f)
{
	m_enForm->addField(f);
	m_faForm->addField(f);
}

void MdiChild::insertField(int index, Field f)
{
	m_enForm->insertField(index, f);
	m_faForm->insertField(index, f);
}

void MdiChild::removeField(Field f)
{
	m_enForm->removeField(f);
	m_faForm->removeField(f);
}

bool MdiChild::isCopyAvailable()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->isCopyAvailable();
}

void MdiChild::copy()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->copy();
}

bool MdiChild::isUndoAvailable()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->isUndoAvailable();
}

void MdiChild::undo()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->undo();
}

bool MdiChild::isRedoAvailable()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->isRedoAvailable();
}

void MdiChild::redo()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->redo();
}

bool MdiChild::currentFieldAcceptsFormatting()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->currentFieldAcceptsFormatting();
}

bool MdiChild::currentFieldAcceptsInsertion()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget())->currentFieldAcceptsInsertion();
}

void MdiChild::setBoldEnabled(bool enabled)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setBoldEnabled(enabled);
	}
}

void MdiChild::setItalicEnabled(bool enabled)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setItalicEnabled(enabled);
	}
}

void MdiChild::setUnderlineEnabled(bool enabled)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setUnderlineEnabled(enabled);
	}
}

void MdiChild::setStrikeThroughEnabled(bool enabled)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setStrikeThroughEnabled(enabled);
	}
}

void MdiChild::removeFormatting()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->removeFormatting();
	}
}

void MdiChild::setTextColor(const QColor &color)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setTextColor(color);
	}
}

void MdiChild::setAutomaticTextColor()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setAutomaticTextColor();
	}
}

void MdiChild::setHighLightColor(const QColor &color)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setHighLightColor(color);
	}
}

void MdiChild::setAutomaticHighLightColor()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setAutomaticHighLightColor();
	}
}

void MdiChild::setLeftAligned()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setLeftAligned();
	}
}

void MdiChild::setRightAligned()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setRightAligned();
	}
}

void MdiChild::setLeftToRight()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setLeftToRight();
	}
}

void MdiChild::setRightToLeft()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->setRightToLeft();
	}
}

void MdiChild::insertTable(int hCells, int vCells)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->insertTable(hCells, vCells);
	}
}

void MdiChild::insertNumberedList()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->insertNumberedList();
	}
}

void MdiChild::insertBulletedList()
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->insertBulletedList();
	}
}

void MdiChild::insertHyperLink(const QString& hyperLink)
{
	EditorWidget	*ew	= currentEditor();

	if (ew)
	{
		ew->insertHyperLink(hyperLink);
	}
}

void MdiChild::removeHyperLink()
{

}

void MdiChild::setMultilineTextEditorsFont(Language l, const QFont& fnt)
{
	switch (l)
	{
	case Language::English:
		m_enForm->setMultilineTextEditorsFont(fnt);

		break;

	case Language::Persian:
		m_faForm->setMultilineTextEditorsFont(fnt);

		break;

	default:

		break;
	}
}

QDomDocument MdiChild::xml(Language l) const
{
	switch (l)
	{
	case Language::English:
		return m_enForm->xml();

	case Language::Persian:
		return m_faForm->xml();

	default:

		break;
	}

	return QDomDocument();
}

QDomDocument MdiChild::xmlTemplate() const
{
	EditorWidget	*ew	{ qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget()) };

	if (ew)
	{
		return ew->xmlTemplate();
	}
	else
	{
		return QDomDocument();
	}
}

void MdiChild::setXmlTemplate(const QDomDocument& doc)
{
	for (int i=0; i<ui->tabWidget->count(); ++i)
	{
		qobject_cast<EditorWidget*>(ui->tabWidget->widget(i))->setXmlTemplate(doc);
	}
}

EditorWidget* MdiChild::currentEditor()
{
	return qobject_cast<EditorWidget*>(ui->tabWidget->currentWidget());
}

void MdiChild::onMoveRowUpRequested(int rowNumber, EditorWidget* /*ew*/)
{
	m_enForm->moveRowUp(rowNumber);
	m_faForm->moveRowUp(rowNumber);
}

void MdiChild::onMoveRowDownRequested(int rowNumber, EditorWidget* /*ew*/)
{
	m_enForm->moveRowDown(rowNumber);
	m_faForm->moveRowDown(rowNumber);
}

void MdiChild::onModified(Language language, Field field)
{
	EditorWidget	*sigEw	{ nullptr },
								*othEw	{ nullptr };

	switch (language)
	{
	case Language::English:
		sigEw	= m_enForm;
		othEw	= m_faForm;

		break;

	case Language::Persian:
		sigEw	= m_faForm;
		othEw	= m_enForm;

		break;

	default:

		break;
	}

	if (sigEw && othEw)
	{
		switch (field)
		{
		case Field::Section:
			othEw->setSection(sigEw->section(), false);

			break;

		case Field::Name:
			othEw->setName(sigEw->name(), false);

			break;

		default:

			break;
		}
	}
}
