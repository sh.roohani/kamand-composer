#include <gui/hyperlinkdialog.hpp>
#include "ui_hyperlinkdialog.h"

HyperLinkDialog::HyperLinkDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::HyperLinkDialog)
{
	ui->setupUi(this);
}

HyperLinkDialog::~HyperLinkDialog()
{
	delete ui;
}

void HyperLinkDialog::populateUi()
{
	ui->targetLineEdit->setText(m_hyperlink);
}

void HyperLinkDialog::collectUi()
{
	m_hyperlink	= ui->targetLineEdit->text();
}
