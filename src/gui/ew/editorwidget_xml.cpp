#include <gui/ew/editorwidget.hpp>

#include <kc_global_funcs.hpp>

#include <QFormLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QTextDocument>
#include <QTextBlock>
#include <QTextFragment>
#include <QTextStream>

#include <gui/keywordswidget.hpp>

#include <QDebug>

QString EditorWidget::colorHexString(const QColor& c)
{
	QString			s;
	QTextStream	ts(&s);

	ts.setIntegerBase(16);
	ts.setFieldWidth(2);
	ts.setPadChar(QChar('0'));
	ts.setNumberFlags(ts.numberFlags() | QTextStream::UppercaseDigits);

	ts << c.red() << c.green() << c.blue();

	return s;
}

QDomElement EditorWidget::textEditElement(Field field, QDomDocument& doc)
{
	QTextEdit		*te	{ qobject_cast<QTextEdit*>(findField(field)) };
	QDomElement	elem;

	if (te)
	{
		QDomElement						blkElem,
													frgElem;
		QDomCDATASection			cdata;
		QDomText							txt;
		QDomNamedNodeMap			attrs;
		QDomAttr							attr;
		QTextDocument					*txtDoc	{ te->document() };
		QTextBlock						block		{ txtDoc->begin() };
		QTextBlock::iterator	it;
		QTextFragment					frag;
		QTextBlockFormat			blkFmt;
		QTextCharFormat				chrFmt;
		QFont									fnt;

		elem	= doc.createElement(fieldToNodeName(field));

		while (block.isValid())
		{
			blkFmt	= block.blockFormat();
			blkElem	= doc.createElement(KC_TAG_BLOCK);
			elem.appendChild(blkElem);
			attrs	= blkElem.attributes();
			attr	= doc.createAttribute(KC_ATTR_DIR);
			attr.setNodeValue(blkFmt.layoutDirection() == Qt::LeftToRight ?
													KC_VAL_LTR : KC_VAL_RTL);
			attrs.setNamedItem(attr);
			attr	= doc.createAttribute(KC_ATTR_ALIGN);
			attr.setNodeValue(blkFmt.alignment() == Qt::AlignLeft ?
													KC_VAL_LEFT : KC_VAL_RIGHT);
			attrs.setNamedItem(attr);

			for (it=block.begin(); !it.atEnd(); ++it)
			{
				frgElem	= doc.createElement(KC_TAG_FRAG);
				blkElem.appendChild(frgElem);
				frag		= it.fragment();
				chrFmt	= frag.charFormat();
				fnt			= chrFmt.font();
				attrs	= frgElem.attributes();
				attr	= doc.createAttribute(KC_ATTR_BOLD);
				attr.setNodeValue(fnt.bold() ? KC_VAL_TRUE : KC_VAL_FALSE);
				attrs.setNamedItem(attr);
				attr	= doc.createAttribute(KC_ATTR_ITALIC);
				attr.setNodeValue(fnt.italic() ? KC_VAL_TRUE : KC_VAL_FALSE);
				attrs.setNamedItem(attr);
				attr	= doc.createAttribute(KC_ATTR_UNDERLINE);
				attr.setNodeValue(fnt.underline() ? KC_VAL_TRUE : KC_VAL_FALSE);
				attrs.setNamedItem(attr);
				attr	= doc.createAttribute(KC_ATTR_STRIKETHROUGH);
				attr.setNodeValue(fnt.strikeOut() ? KC_VAL_TRUE : KC_VAL_FALSE);
				attrs.setNamedItem(attr);
				attr	= doc.createAttribute(KC_ATTR_TXT_COLOR);
				attr.setNodeValue(QString("#%1").arg(colorHexString(chrFmt.foreground().color())));
				attrs.setNamedItem(attr);
				attr	= doc.createAttribute(KC_ATTR_HLT_COLOR);
				attr.setNodeValue(QString("#%1").arg(colorHexString(chrFmt.background().color())));
				attrs.setNamedItem(attr);

				cdata	= doc.createCDATASection(te->toPlainText());
				frgElem.appendChild(cdata);
			}

			block	= block.next();
		}
	}

	return elem;
}

QDomElement EditorWidget::keywordsElement(QDomDocument& doc)
{
	KeywordsWidget		*kw	{ qobject_cast<KeywordsWidget*>(findField(Field::Keywords)) };
	QDomElement				elem,
										kwElem;
	QDomCDATASection	cdata;

	if (kw)
	{
		QStringList	kwList	= kw->keywords();

		elem	= doc.createElement(fieldToNodeName(Field::Keywords));

		for (const QString& k: kwList)
		{
			kwElem	= doc.createElement(KC_TAG_KEYWORD);
			elem.appendChild(kwElem);
			cdata	= doc.createCDATASection(k);
			kwElem.appendChild(cdata);
		}
	}

	return elem;
}

QDomDocument EditorWidget::xml()
{
	QDomDocument			doc;
	QDomElement				docElem	{ doc.createElement("doc") },
										elem;
	QDomText					txt;
	QDomNamedNodeMap	attrs;
	QDomAttr					attr;
	Field							f;

	doc.appendChild(doc.createProcessingInstruction("xml", R"(version="1.0" encoding="UTF-8")"));
	doc.appendChild(docElem);

	for (int i=0; i<m_formLayout->rowCount(); ++i)
	{
		switch ((f = rowType(i)))
		{
		case Field::Section:
		{
			QComboBox	*cbx	= qobject_cast<QComboBox*>(rowWidget(i));

			attr	= doc.createAttribute(fieldToNodeName(f));
			attr.setValue(QString::number(cbx->currentIndex() + 1));
			attrs	= docElem.attributes();
			attrs.setNamedItem(attr);
		}
			break;

		case Field::Name:
			// Fall-through
		case Field::Author:
		{
			QLineEdit	*le	= qobject_cast<QLineEdit*>(rowWidget(i));

			elem	= doc.createElement(fieldToNodeName(f));
			txt	= doc.createTextNode(le->text());
			elem.appendChild(txt);
			docElem.appendChild(elem);
		}

			break;

		case Field::Synopsis:
			// fall-through
		case Field::Description:
			// fall-through
		case Field::ReturnValue:
			// fall-through
		case Field::Errors:
			// Fall-through
		case Field::Attributes:
			// Fall-through
		case Field::ConformingTo:
			// Fall-through
		case Field::Notes:
			// Fall-through
		case Field::Bugs:
			// Fall-through
		case Field::ReportingBugs:
			// Fall-through
		case Field::SeeAlso:
			// Fall-through
		case Field::Colophon:
			// Fall-through
		case Field::Copyright:
			elem	= textEditElement(f, doc);
			docElem.appendChild(elem);

			break;

		case Field::Keywords:
			elem	= keywordsElement(doc);
			docElem.appendChild(elem);

			break;

		default:

			break;
		}
	}

	return doc;
}

QDomDocument EditorWidget::xmlTemplate()
{
	QDomDocument			doc;
	QDomElement				docElem	{ doc.createElement("doc") },
										elem;
	QDomNamedNodeMap	attrs;
	QDomAttr					attr;
	Field							field;
	QString						fieldName;

	doc.appendChild(doc.createProcessingInstruction("xml", R"(version="1.0" encoding="UTF-8")"));
	doc.appendChild(docElem);

	visitRowWidgets([&](QWidget* widget) -> bool
	{
		field			= rowType(widget);
		fieldName	= fieldToNodeName(field);

		if (field == Field::Section)
		{
			QComboBox	*cbx	= qobject_cast<QComboBox*>(widget);

			attr	= doc.createAttribute(fieldName);
			attr.setValue(QString::number(cbx->currentIndex() + 1));
			attrs	= docElem.attributes();
			attrs.setNamedItem(attr);
		}
		else
		{
			elem	= doc.createElement(fieldName);
			docElem.appendChild(elem);
		}

		return true;
	});

	return doc;
}

void EditorWidget::setXmlTemplate(const QDomDocument& doc)
{
	QDomNodeList			childNodes	{ doc.childNodes() };
	QDomNode					docNode,
										rowNode,
										attr;
	QDomNamedNodeMap	attrs;
	QString						name				{ fieldToNodeName(Field::Section) };
	int								sect				{ 1 };

	for (int i=0; i<childNodes.count(); ++i)
	{
		docNode	= childNodes.at(i);

		if (docNode.nodeName() == "doc")
		{
			break;
		}
	}

	attrs	= docNode.attributes();
	if (attrs.contains(name))
	{
		attr	= attrs.namedItem(name);
		sect	= attr.nodeValue().toInt();
		if (sect < 1 || sect > 8)
		{
			sect	= 1;
		}
	}

	addField(Field::Section);
	setSection(sect, true);
	qobject_cast<QComboBox*>(findField(Field::Section))
			->setProperty(qPrintable(PROPERTY_INIT_STATE), false);

	childNodes	= docNode.childNodes();
	for (int i=0; i<childNodes.count(); ++i)
	{
		rowNode	= childNodes.at(i);

		addField(nodeNameToField(rowNode.nodeName()));
	}

	if (fieldRowNumber(Field::Name) < 0)
	{
		addField(Field::Name);
	}

	if (fieldRowNumber(Field::Synopsis) < 0)
	{
		addField(Field::Synopsis);
	}

	if (fieldRowNumber(Field::Description) < 0)
	{
		addField(Field::Description);
	}

	applyMlteFont();
}
