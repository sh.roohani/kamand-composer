#include <gui/ew/editorwidget.hpp>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QScrollArea>
#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <QListWidget>
#include <QKeyEvent>
#include <QPalette>

#include <QTextDocument>
#include <QTextCursor>
#include <QTextDocumentFragment>
#include <QTextCharFormat>

#include <QDebug>

const QString EditorWidget::PROPERTY_INIT_STATE			= "InitState";
const QString EditorWidget::PROPERTY_FIELD_TYPE			= "FieldType";
const QString EditorWidget::PROPERTY_COPY_AVAILABLE	= "CopyAvailable";
const QString EditorWidget::PROPERTY_UNDO_AVAILABLE	= "UndoAvailable";
const QString EditorWidget::PROPERTY_REDO_AVAILABLE	= "RedoAvailable";

EditorWidget::EditorWidget(Language lang, QWidget *parent) :
	QWidget(parent),
	m_lang(lang)
{
	QVBoxLayout	*vboxLayout	= new QVBoxLayout();
	setLayout(vboxLayout);

	m_scrollArea	= new QScrollArea();
	m_scrollArea->setWidgetResizable(true);
	m_scrollArea->setSizeAdjustPolicy(QScrollArea::AdjustToContents);
	vboxLayout->addWidget(m_scrollArea);

	m_formLayout	= new QFormLayout();

	QWidget	*containerWidget	= new QWidget();
	m_scrollArea->setWidget(containerWidget);
	containerWidget->setLayout(m_formLayout);
}

void EditorWidget::setClean()
{
	m_modified	= false;
}

void EditorWidget::setDocumentName(const QString& name)
{
	m_docName	= name;
}

void EditorWidget::setName(const QString& name, bool doSignal)
{
	QLineEdit	*le	= qobject_cast<QLineEdit*>(findField(Field::Name));

	if (le)
	{
		if (!doSignal)
		{
			le->blockSignals(true);
		}

		le->setText(name);

		if (!doSignal)
		{
			le->blockSignals(false);
		}
	}
}

QString EditorWidget::name()
{
	QLineEdit	*le	= qobject_cast<QLineEdit*>(findField(Field::Name));

	if (le)
	{
		return le->text();
	}
	else
	{
		return QString();
	}
}

void EditorWidget::setSection(Section section, bool doSignal)
{
	if (section > Section::Unknown && section < Section::Last)
	{
		setSection(static_cast<int>(section), doSignal);
	}
}

Section EditorWidget::section()
{
	QComboBox	*cb	= qobject_cast<QComboBox*>(findField(Field::Section));

	if (cb)
	{
		return static_cast<Section>(cb->currentIndex() + 1);
	}
	else
	{
		return Section::Unknown;
	}
}

QList<Field> EditorWidget::fields()
{
	QList<Field>	fieldsList;

	visitRowWidgets([&](QWidget* widget)
	{
		fieldsList << rowType(widget);

		return true;
	});

	return fieldsList;
}

void EditorWidget::setSection(int section, bool doSignal)
{
	QComboBox	*cb	= qobject_cast<QComboBox*>(findField(Field::Section));

	if (cb)
	{
		if (!doSignal)
		{
			cb->blockSignals(true);
		}

		cb->setCurrentIndex(section - 1);

		if (!doSignal)
		{
			cb->blockSignals(false);
		}
	}
}

void EditorWidget::setModified(QWidget* widget, bool state)
{
	QVariant	v					= widget ?
													widget->property(qPrintable(PROPERTY_FIELD_TYPE)) :
													QVariant();
	Field			f					= v.isValid() ? v.value<Field>() : Field::Unknown;
	bool			prevState	= m_modified;

	m_modified	= state;

	if (f == Field::Section || f == Field::Name || !prevState)
	{
		emit modified(m_lang, f);
	}
}

void EditorWidget::setBoldEnabled(bool enabled)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setFontWeight(enabled ? QFont::Bold : QFont::Normal);

		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setItalicEnabled(bool enabled)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setFontItalic(enabled);

		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setUnderlineEnabled(bool enabled)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setFontUnderline(enabled);

		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setStrikeThroughEnabled(bool enabled)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setFontStrikeOut(enabled);
		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::removeFormatting()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor	cur	{ te->textCursor() };

		if (cur.hasSelection())
		{
			QTextCharFormat	f	{ cur.charFormat() };

			f.setFontWeight(QFont::Normal);
			f.setFontItalic(false);
			f.setFontUnderline(false);
			f.setFontStrikeOut(false);

			cur.mergeCharFormat(f);
		}
	}
}

void EditorWidget::setTextColor(const QColor &color)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setForeground(color);
		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setAutomaticTextColor()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setForeground(te->palette().color(QPalette::Text));
		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setHighLightColor(const QColor &color)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setBackground(color);
		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setAutomaticHighLightColor()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor			cur		{ te->textCursor() };
		QTextCharFormat	f			{ };

		f.setBackground(te->palette().color(QPalette::Base));
		if (cur.hasSelection())
		{
			cur.mergeCharFormat(f);
		}
		else
		{
			te->setCurrentCharFormat(f);
		}
	}
}

void EditorWidget::setLeftAligned()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor				cur		{ te->textCursor() };
		QTextBlockFormat	f			{ };

		f.setAlignment(Qt::AlignLeft);
		cur.mergeBlockFormat(f);
	}
}

void EditorWidget::setRightAligned()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor				cur		{ te->textCursor() };
		QTextBlockFormat	f			{ };

		f.setAlignment(Qt::AlignRight);
		cur.mergeBlockFormat(f);
	}
}

void EditorWidget::setLeftToRight()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor	cur	{ te->textCursor() };

		if (cur.hasSelection())
		{
			QString	text	{ cur.selectedText() };

			text.prepend("\u202a"); // Left to Right Embedding
			text.append("\u202c"); // Pop Directional Formatting
			cur.insertText(text);
		}
		else
		{
			QTextBlockFormat	f;

			f.setLayoutDirection(Qt::LeftToRight);
			cur.mergeBlockFormat(f);
		}
	}
}

void EditorWidget::setRightToLeft()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor				cur		{ te->textCursor() };

		if (cur.hasSelection())
		{
			QString	text	{ cur.selectedText() };

			text.prepend("\u202b"); // Right to Left Embedding
			text.append("\u202c"); // Pop Directional Formatting
			cur.insertText(text);
		}
		else
		{
			QTextBlockFormat	f;

			f.setLayoutDirection(Qt::RightToLeft);
			cur.mergeBlockFormat(f);
		}
	}
}

void EditorWidget::insertTable(int hCells, int vCells)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor	cur	{ te->textCursor() };

		cur.insertTable(vCells, hCells);
	}
}

void EditorWidget::insertNumberedList()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor	cur	{ te->textCursor() };

		cur.insertList(QTextListFormat::ListDecimal);
	}
}

void EditorWidget::insertBulletedList()
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor	cur	{ te->textCursor() };

		cur.insertList(QTextListFormat::ListCircle);
	}
}

void EditorWidget::insertHyperLink(const QString& hyperLink)
{
	QTextEdit	*te	= qobject_cast<QTextEdit*>(focusWidget());

	if (te)
	{
		QTextCursor	cur	{ te->textCursor() };

		if (cur.hasSelection())
		{
			QString	selText	= cur.selectedText();

			selText.prepend(QString("<a href=\"%1\">").arg(hyperLink));
			selText.append("</a>");

			cur.insertHtml(selText);
		}
	}
}

void EditorWidget::removeHyperLink()
{

}

void EditorWidget::setMultilineTextEditorsFont(const QFont &fnt)
{
	m_mlteFont	= fnt;
}

void EditorWidget::showEvent(QShowEvent* /*event*/)
{
	QWidget	*w	= findField(Field::Name);
	if (w)
	{
		w->setFocus();
		emit currentFocusChanged(QTextCharFormat(), false);
	}
}

void EditorWidget::applyMlteFont()
{
	QTextEdit				*te	{ nullptr };
	QTextCharFormat	f;

	for (int i=0; i<m_formLayout->rowCount(); ++i)
	{
		if ((te	= qobject_cast<QTextEdit*>(qobject_cast<QHBoxLayout*>(
					m_formLayout->itemAt(i, QFormLayout::FieldRole)->layout())
					->itemAt(0)->widget())))
		{
			f	= te->currentCharFormat();

			f.setFont(m_mlteFont);
			te->setCurrentCharFormat(f);
		}
	}
}

bool EditorWidget::eventFilter(QObject* watched, QEvent* event)
{
	switch (event->type())
	{
	case QEvent::FocusIn:
	{
		QTextEdit	*te	= qobject_cast<QTextEdit*>(watched);

		emit currentFocusChanged(te ? te->textCursor().charFormat() : QTextCharFormat(),
														 te);

		qDebug() << watched->isWidgetType();
		if (watched->isWidgetType())
		{
			m_scrollArea->ensureWidgetVisible(qobject_cast<QWidget*>(watched));
			qDebug() << "HERE";
		}
	}

		break;

	default:

		break;
	}

	return QWidget::eventFilter(watched, event);
}
