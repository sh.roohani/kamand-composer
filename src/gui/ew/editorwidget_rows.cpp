#include <gui/ew/editorwidget.hpp>

#include <QFormLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <gui/keywordswidget.hpp>
#include <gui/updownbuttonswidget.hpp>

void EditorWidget::moveRowUp(int rowNumber)
{
	if (rowNumber > 0)
	{
		QFormLayout::TakeRowResult	r			= m_formLayout->takeRow(rowNumber);

		m_formLayout->insertRow(rowNumber - 1, r.labelItem->widget(),
														r.fieldItem->layout());

		updateRowNumbers();
	}
}

void EditorWidget::moveRowDown(int rowNumber)
{
	if (rowNumber < m_formLayout->rowCount() - 1)
	{
		QFormLayout::TakeRowResult	r	= m_formLayout->takeRow(rowNumber);

		m_formLayout->insertRow(rowNumber + 1, r.labelItem->widget(),
														r.fieldItem->layout());
	}

	updateRowNumbers();
}

int EditorWidget::addField(Field f)
{
	int	rowCount	= m_formLayout->rowCount();

	insertField(rowCount, f);

	return rowCount;
}

void EditorWidget::insertField(int index, Field f)
{
	QLineEdit	*le		{ };
	QTextEdit	*te		{ };
	QString		title	{ fieldString(f) };

	switch (f)
	{
	case Field::Section:
	{
		QComboBox		*sectionsComboBox	= new QComboBox();
		QStringList	items;

		for (Section s=Section::Section1; s<Section::Last; ++s)
		{
			items << sectionString(s);
		}

		sectionsComboBox->blockSignals(true);
		prepareComboBoxConnections(sectionsComboBox);
		sectionsComboBox->addItems(items);
		sectionsComboBox->setCurrentIndex(-1);
		sectionsComboBox->setProperty(qPrintable(PROPERTY_INIT_STATE), true);
		sectionsComboBox->blockSignals(false);
		addRow(index, title, sectionsComboBox, f);
	}

		break;

	case Field::Name:
		le	= new QLineEdit();

		addRow(index, title, le, f);
		prepareLineEditConnections(le);

		break;

	case Field::Synopsis:
		te	= new QTextEdit();

		te->setMinimumHeight(80);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Description:
		te	= new QTextEdit();

		te->setMinimumHeight(600);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::ReturnValue:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Errors:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Attributes:
		te	= new QTextEdit();

		te->setMinimumHeight(250);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Author:
		le	= new QLineEdit();
		addRow(index, title, le, f);
		prepareLineEditConnections(le);

		break;

	case Field::ConformingTo:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Notes:
		te	= new QTextEdit();

		te->setMinimumHeight(250);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Bugs:
		te	= new QTextEdit();

		te->setMinimumHeight(250);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::ReportingBugs:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::SeeAlso:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Colophon:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Copyright:
		te	= new QTextEdit();

		te->setMinimumHeight(120);
		addRow(index, title, te, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareTextEditConnections(te);

		break;

	case Field::Keywords:
	{
		KeywordsWidget	*kw	= new KeywordsWidget();

		kw->setMinimumHeight(200);
		addRow(index, title, kw, f,
					 QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
		prepareKeywordsWidgetConnections(kw);
	}

		break;

	default:

		break;
	}

	updateRowNumbers();
}

void EditorWidget::removeField(Field f)
{
	int			rowNumber	{ -1 };
	QWidget	*widget		{ findField(f, &rowNumber) };

	if (widget)
	{
		m_formLayout->removeRow(rowNumber);
	}

	updateRowNumbers();
}

bool EditorWidget::isCopyAvailable()
{
	return currentWidget()->property(qPrintable(PROPERTY_COPY_AVAILABLE)).toBool();
}

void EditorWidget::copy()
{
	QWidget		*w	= currentWidget();
	QTextEdit	*te	= qobject_cast<QTextEdit*>(w);
	QLineEdit	*le	= qobject_cast<QLineEdit*>(w);

	if (te)
	{
		te->copy();
	}
	else if (le)
	{
		le->copy();
	}
}

bool EditorWidget::isUndoAvailable()
{
	return currentWidget()->property(qPrintable(PROPERTY_UNDO_AVAILABLE)).toBool();
}

void EditorWidget::undo()
{
	QWidget		*w	= currentWidget();
	QTextEdit	*te	= qobject_cast<QTextEdit*>(w);
	QLineEdit	*le	= qobject_cast<QLineEdit*>(w);

	if (te)
	{
		te->undo();
	}
	else if (le)
	{
		le->undo();
	}
}

bool EditorWidget::isRedoAvailable()
{
	return currentWidget()->property(qPrintable(PROPERTY_REDO_AVAILABLE)).toBool();
}

void EditorWidget::redo()
{
	QWidget		*w	= currentWidget();
	QTextEdit	*te	= qobject_cast<QTextEdit*>(w);
	QLineEdit	*le	= qobject_cast<QLineEdit*>(w);

	if (te)
	{
		te->redo();
	}
	else if (le)
	{
		le->redo();
	}
}

bool EditorWidget::currentFieldAcceptsFormatting()
{
	return qobject_cast<QTextEdit*>(focusWidget());
}

bool EditorWidget::currentFieldAcceptsInsertion()
{
	return qobject_cast<QTextEdit*>(focusWidget());
}

void EditorWidget::addRow(int rowNumber,
													const QString& labelText,
													QWidget* field,
													Field fieldType,
													QSizePolicy::Policy horizontal,
													QSizePolicy::Policy vertical)
{
	UpDownButtonsWidget	*udbw		{ new UpDownButtonsWidget(rowNumber) };
	QLabel							*label	{ new QLabel(labelText) };
	QHBoxLayout					*hbl		{ new QHBoxLayout() };
	QVariant						v;
	QTextEdit						*te			{ qobject_cast<QTextEdit*>(field) };

	if (te)
	{
		te->setContextMenuPolicy(Qt::CustomContextMenu);
	}

	field->installEventFilter(this);
	udbw->installEventFilter(this);
	udbw->setFocusPolicy(Qt::NoFocus);

	v.setValue<Field>(fieldType);
	field->setProperty(qPrintable(PROPERTY_FIELD_TYPE), v);
	field->setProperty(qPrintable(PROPERTY_COPY_AVAILABLE), false);
	field->setProperty(qPrintable(PROPERTY_UNDO_AVAILABLE), false);
	field->setProperty(qPrintable(PROPERTY_REDO_AVAILABLE), false);
	field->setSizePolicy(horizontal, vertical);

	hbl->addWidget(field);
	hbl->addWidget(udbw);
	label->setBuddy(field);

	connect(udbw, &UpDownButtonsWidget::upClicked,
					this, &EditorWidget::onUpClicked);

	connect(udbw, &UpDownButtonsWidget::downClicked,
					this, &EditorWidget::onDownClicked);

	m_formLayout->insertRow(rowNumber, label, hbl);
}

void EditorWidget::updateRowNumbers()
{
	for (int i=0; i<m_formLayout->rowCount(); ++i)
	{
		rowUpDownButtonsWidget(i)->setRownNumber(i);
	}
}

Field EditorWidget::rowType(int rowNumber)
{
	return rowWidget(rowNumber)
			->property(qPrintable(PROPERTY_FIELD_TYPE)).value<Field>();
}

Field EditorWidget::rowType(QWidget* widget)
{
	return widget->property(qPrintable(PROPERTY_FIELD_TYPE)).value<Field>();
}

int EditorWidget::fieldRowNumber(Field f)
{
	int	rowNumber	= -1;

	for (int i=0; i<m_formLayout->rowCount(); ++i)
	{
		if (rowType(i) == f)
		{
			rowNumber	= i;

			break;
		}
	}

	return rowNumber;
}

QWidget *EditorWidget::rowWidget(int rowNumber)
{
	return qobject_cast<QWidget*>(qobject_cast<QHBoxLayout*>(
				m_formLayout->itemAt(rowNumber, QFormLayout::FieldRole)->layout())
																->itemAt(0)->widget());
}

QWidget* EditorWidget::currentWidget()
{
	QWidget* widget	= nullptr;

	visitRowWidgets([&](QWidget* w) -> bool
	{
		if (w->hasFocus())
		{
			widget	= w;

			return false;
		}
		else
		{
			return true;
		}
	});

	return widget;
}

QWidget* EditorWidget::findField(Field f, int *rowNumber)
{
	QHBoxLayout	*hbl	{ nullptr };
	QWidget			*w		{ nullptr };
	QVariant		v;

	for (int i=0; i<m_formLayout->rowCount(); ++i)
	{
		hbl		= qobject_cast<QHBoxLayout*>(m_formLayout->itemAt(i, QFormLayout::FieldRole)->layout());
		w	= qobject_cast<QWidget*>(hbl->itemAt(0)->widget());

		if ((v = w->property(qPrintable(PROPERTY_FIELD_TYPE))).value<Field>() == f)
		{
			if (rowNumber)
			{
				*rowNumber	= i;
			}

			break;
		}
	}

	return w;
}

UpDownButtonsWidget *EditorWidget::rowUpDownButtonsWidget(int rowNumber)
{
	return qobject_cast<UpDownButtonsWidget*>(qobject_cast<QHBoxLayout*>(
				m_formLayout->itemAt(rowNumber, QFormLayout::FieldRole)->layout())
																						->itemAt(1)->widget());
}

void EditorWidget::visitRowWidgets(std::function<bool(QWidget*)> visitor)
{
	for (int i=0; i<m_formLayout->rowCount(); ++i)
	{
		if (!visitor(rowWidget(i)))
		{
			break;
		}
	}
}

void EditorWidget::onUpClicked(int rowNumber)
{
	emit moveRowUpRequested(rowNumber, this);
}

void EditorWidget::onDownClicked(int rowNumber)
{
	emit moveRowDownRequested(rowNumber, this);
}
