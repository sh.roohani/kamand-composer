#include <gui/ew/editorwidget.hpp>

#include <QComboBox>
#include <QLineEdit>
#include <QTextEdit>
#include <gui/keywordswidget.hpp>

#include <QDebug>

void EditorWidget::prepareComboBoxConnections(QComboBox* cb)
{
	connect(cb, QOverload<int>::of(&QComboBox::currentIndexChanged),
					[=](int)
	{
		setModified(cb, !cb->property(qPrintable(PROPERTY_INIT_STATE)).toBool());
	});
}

void EditorWidget::prepareLineEditConnections(QLineEdit* le)
{
	connect(le, &QLineEdit::textChanged, [=](const QString&)
	{
		bool	undoAvail	{ le->isUndoAvailable() },
					redoAvail	{ le->isRedoAvailable() };

		setModified(le, true);

		le->setProperty(qPrintable(PROPERTY_UNDO_AVAILABLE), undoAvail);
		le->setProperty(qPrintable(PROPERTY_REDO_AVAILABLE), redoAvail);
		emit undoAvailable(le, undoAvail);
		emit redoAvailable(le, redoAvail);
	});

	connect(le, &QLineEdit::selectionChanged, [=]()
	{
		bool	available	{ le->hasSelectedText() ? true : false };

		le->setProperty(qPrintable(PROPERTY_COPY_AVAILABLE), available);
		emit copyAvailable(le, available);
	});
}

void EditorWidget::prepareTextEditConnections(QTextEdit* te)
{
	te->setContextMenuPolicy(Qt::CustomContextMenu);

	connect(te, &QTextEdit::customContextMenuRequested, [=](const QPoint& pt)
	{
		showCustomContextMenu(te, pt);
	});

	connect(te, &QTextEdit::currentCharFormatChanged,
					[=](const QTextCharFormat &format)
	{
		setModified(te, true);

		emit currentCharFormatChanged(format);
	});

	connect(te, &QTextEdit::textChanged, [=]()
	{
		setModified(te, true);
	});

	connect(te, &QTextEdit::copyAvailable, [=](bool yes)
	{
		te->setProperty(qPrintable(PROPERTY_COPY_AVAILABLE), yes);
		emit copyAvailable(te, yes);
	});

	connect(te, &QTextEdit::undoAvailable, [=](bool yes)
	{
		te->setProperty(qPrintable(PROPERTY_UNDO_AVAILABLE), yes);
		emit undoAvailable(te, yes);
	});

	connect(te, &QTextEdit::redoAvailable, [=](bool yes)
	{
		te->setProperty(qPrintable(PROPERTY_REDO_AVAILABLE), yes);
		emit redoAvailable(te, yes);
	});
}

void EditorWidget::prepareKeywordsWidgetConnections(KeywordsWidget* kw)
{
	connect(kw, &KeywordsWidget::modified, [=]()
	{
		setModified(kw, true);
	});
}



