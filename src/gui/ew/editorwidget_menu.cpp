#include <gui/ew/editorwidget.hpp>

#include <QTextEdit>
#include <QMenu>
#include <QTextCursor>
#include <QTextTable>
#include <QTextTableCell>
#include <QTextList>
#include <QAction>

#include <gui/tbl/tableinsertiondialog.hpp>

QMenu* EditorWidget::textTableMenu(QTextEdit* te, const QPoint& pt)
{
	QMenu				*menu			{ te->createStandardContextMenu() },
							*subMenu	{  };
	QTextCursor	cur				{ te->textCursor() };
	QTextTable	*tbl			{ cur.currentTable() };
	int					rBeg			{ },
							rCnt			{ },
							cBeg			{ },
							cCnt			{ };
	QAction			*act			{ };

	cur.selectedTableCells(&rBeg, &rCnt, &cBeg, &cCnt);

	menu->addSeparator();

	subMenu	= new QMenu(tr("Insert"));
	menu->addMenu(subMenu);
	act	= subMenu->addAction(tr("Rows Above"));
	connect(act, &QAction::triggered, [=](bool)
	{
		QTextTableCell	cell	= tbl->cellAt(cur);

		tbl->insertRows(cell.row(), 1);
	});
	act	= subMenu->addAction(tr("Rows Below"));
	connect(act, &QAction::triggered, [=](bool)
	{
		QTextTableCell	cell	= tbl->cellAt(cur);

		tbl->insertRows(cell.row() + 1, 1);
	});
	act	= subMenu->addAction(tr("Rows..."));
	connect(act, &QAction::triggered, [=](bool)
	{
		TableInsertionDialog	dlg;

		dlg.setWindowTitle(tr("Insert Rows"));
		if (dlg.exec() == QDialog::Accepted)
		{
			QTextTableCell	cell	= tbl->cellAt(cur);

			dlg.collectUi();
			tbl->insertRows(dlg.m_before ? cell.row() :
																		 cell.row() + 1, dlg.m_number);
		}
	});
	subMenu->addSeparator();
	act	= subMenu->addAction(tr("Columns Left"));
	connect(act, &QAction::triggered, [=](bool)
	{
		QTextTableCell	cell	= tbl->cellAt(cur);

		tbl->insertColumns(cell.column(), 1);
	});
	act	= subMenu->addAction(tr("Columns Right"));
	connect(act, &QAction::triggered, [=](bool)
	{
		QTextTableCell	cell	= tbl->cellAt(cur);

		tbl->insertColumns(cell.column() + 1, 1);
	});
	act	= subMenu->addAction(tr("Columns..."));
	connect(act, &QAction::triggered, [=](bool)
	{
		TableInsertionDialog	dlg;

		dlg.setWindowTitle(tr("Insert Columns"));
		if (dlg.exec() == QDialog::Accepted)
		{
			QTextTableCell	cell	= tbl->cellAt(cur);

			dlg.collectUi();
			tbl->insertColumns(dlg.m_before ? cell.column() :
																				cell.column() + 1, dlg.m_number);
		}
	});
	subMenu	= new QMenu(tr("Delete"));
	menu->addMenu(subMenu);
	act	= subMenu->addAction(tr("Rows"));
	connect(act, &QAction::triggered, [=](bool)
	{
		if (rCnt > 0)
		{
			tbl->removeRows(rBeg, rCnt);
		}
		else
		{
			QTextTableCell	cell	= tbl->cellAt(cur);

			tbl->removeRows(cell.row(), 1);
		}
	});
	act	= subMenu->addAction(tr("Columns"));
	connect(act, &QAction::triggered, [=](bool)
	{
		if (cCnt > 0)
		{
			tbl->removeColumns(cBeg, cCnt);
		}
		else
		{
			QTextTableCell	cell	= tbl->cellAt(cur);

			tbl->removeColumns(cell.column(), 1);
		}
	});
	act	= subMenu->addAction(tr("Table"));
	connect(act, &QAction::triggered, [=](bool)
	{
		tbl->removeRows(0, tbl->rows());
	});

	return menu;
}

QMenu* EditorWidget::textListMenu(QTextEdit* te, const QPoint& pt)
{
	QMenu				*menu	{ te->createStandardContextMenu() };
	QTextCursor	cur		{ te->textCursor() };
	QTextList		*lst	{ cur.currentList() };

	return menu;
}

QMenu* EditorWidget::standardMenu(QTextEdit* te, const QPoint& pt)
{
	QMenu	*menu	{ te->createStandardContextMenu() };

	return menu;
}

void EditorWidget::showCustomContextMenu(QTextEdit* te, const QPoint& pt)
{
	QTextCursor	cur		{ te->textCursor() };
	QTextTable	*tbl	{ cur.currentTable() };
	QTextList		*lst		{ cur.currentList() };
	QMenu				*menu	{ };
	if (tbl)
	{
		menu	= textTableMenu(te, pt);
	}
	else if (lst)
	{
		menu	= textListMenu(te, pt);
	}
	else
	{
		menu	= standardMenu(te, pt);
	}

	menu->exec(te->mapToGlobal(pt));

	delete menu;
}
