#include <gui/ew/editorwidget.hpp>

#include <kc_global_vars.hpp>

QString EditorWidget::fieldString(Field f)
{
	switch (m_lang)
	{
	case Language::English:
		switch (f)
		{
		case Field::Section:
			return tr("&Section");

		case Field::Name:
			return tr("&Name");

		case Field::Synopsis:
			return tr("S&ynopsis");

		case Field::Description:
			return tr("&Description");

		case Field::ReturnValue:
			return tr("&Return Value");

		case Field::Errors:
			return tr("&Errors");

		case Field::Attributes:
			return tr("A&ttributes");

		case Field::Author:
			return tr("&Author");

		case Field::ConformingTo:
			return tr("&Conforming To");

		case Field::Notes:
			return tr("&Notes");

		case Field::Bugs:
			return tr("&Bugs");

		case Field::ReportingBugs:
			return tr("Re&porting Bugs");

		case Field::SeeAlso:
			return tr("&See Also");

		case Field::Colophon:
			return tr("Co&lophon");

		case Field::Copyright:
			return tr("Copyri&ght");

		case Field::Keywords:
			return tr("&Keywords");

		default:

			break;
		}

		break;

	case Language::Persian:
		switch (f)
		{
		case Field::Section:
			return g_faTr.translate(metaObject()->className(), "&Section");

		case Field::Name:
			return g_faTr.translate(metaObject()->className(), "&Name");

		case Field::Synopsis:
			return g_faTr.translate(metaObject()->className(), "S&ynopsis");

		case Field::Description:
			return g_faTr.translate(metaObject()->className(), "&Description");

		case Field::ReturnValue:
			return g_faTr.translate(metaObject()->className(), "&Return Value");

		case Field::Errors:
			return g_faTr.translate(metaObject()->className(), "&Errors");

		case Field::Attributes:
			return g_faTr.translate(metaObject()->className(), "A&ttributes");

		case Field::Author:
			return g_faTr.translate(metaObject()->className(), "&Author");

		case Field::ConformingTo:
			return g_faTr.translate(metaObject()->className(), "&Conforming To");

		case Field::Notes:
			return g_faTr.translate(metaObject()->className(), "&Notes");

		case Field::Bugs:
			return g_faTr.translate(metaObject()->className(), "&Bugs");

		case Field::ReportingBugs:
			return g_faTr.translate(metaObject()->className(), "Re&porting Bugs");

		case Field::SeeAlso:
			return g_faTr.translate(metaObject()->className(), "&See Also");

		case Field::Colophon:
			return g_faTr.translate(metaObject()->className(), "Co&lophon");

		case Field::Copyright:
			return g_faTr.translate(metaObject()->className(), "Copyri&ght");

		case Field::Keywords:
			return g_faTr.translate(metaObject()->className(), "&Keywords");

		default:

			break;
		}

		break;

	default:

		break;
	}

	return QString();
}

QString EditorWidget::sectionString(Section s)
{
	switch (m_lang)
	{
	case Language::English:
		switch (s)
		{
		case Section::Section1:
			return tr("1 - Commands available to users");

		case Section::Section2:
			return tr("2 - Unix and C system calls");

		case Section::Section3:
			return tr("3 - C library routines for C programs");

		case Section::Section4:
			return tr("4 - Special file names");

		case Section::Section5:
			return tr("5 - File formats and conventions for files used by Unix");

		case Section::Section6:
			return tr("6 - Games");

		case Section::Section7:
			return tr("7 - Word processing packages");

		case Section::Section8:
			return tr("8 - System administration commands and procedures");

		default:

			break;
		}

		break;

	case Language::Persian:
		switch (s)
		{
		case Section::Section1:
			return g_faTr.translate(metaObject()->className(), "1 - Commands available to users");

		case Section::Section2:
			return g_faTr.translate(metaObject()->className(), "2 - Unix and C system calls");

		case Section::Section3:
			return g_faTr.translate(metaObject()->className(), "3 - C library routines for C programs");

		case Section::Section4:
			return g_faTr.translate(metaObject()->className(), "4 - Special file names");

		case Section::Section5:
			return g_faTr.translate(metaObject()->className(), "5 - File formats and conventions for files used by Unix");

		case Section::Section6:
			return g_faTr.translate(metaObject()->className(), "6 - Games");

		case Section::Section7:
			return g_faTr.translate(metaObject()->className(), "7 - Word processing packages");

		case Section::Section8:
			return g_faTr.translate(metaObject()->className(), "8 - System administration commands and procedures");

		default:

			break;
		}

		break;

	default:

		break;
	}

	return QString();
}
