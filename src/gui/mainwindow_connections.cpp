#include <gui/mainwindow.hpp>
#include "ui_mainwindow.h"

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QToolButton>
#include <QMessageBox>

#include <gui/cpw/colorpickerwidget.hpp>
#include <gui/tbl/tablemakerwidget.hpp>

void MainWindow::prepareFileMenuConnections()
{
	connect(ui->actionFileNewDocument, &QAction::triggered,
					this, &MainWindow::onFileNewDocument);

	connect(ui->actionFileNewFromTemplate, &QAction::triggered,
					this, &MainWindow::onFileNewFromTemplate);

	connect(ui->actionFileOpen, &QAction::triggered,
					this, &MainWindow::onFileOpen);

	connect(ui->actionFileClose, &QAction::triggered,
					this, &MainWindow::onFileClose);

	connect(ui->actionFileCloseAll, &QAction::triggered,
					this, &MainWindow::onFileCloseAll);

	connect(ui->actionFileSave, &QAction::triggered,
					this, &MainWindow::onFileSave);

	connect(ui->actionFileSaveAsDocument, &QAction::triggered,
					this, &MainWindow::onFileSaveAsDocument);

	connect(ui->actionFileSaveAsTemplate, &QAction::triggered,
					this, &MainWindow::onFileSaveAsTemplate);

	connect(ui->actionFileSaveAll, &QAction::triggered,
					this, &MainWindow::onFileSaveAll);

	connect(ui->actionFileExit, &QAction::triggered,
					this, &MainWindow::onFileExit);
}

void MainWindow::prepareEditMenuConnections()
{
	connect(ui->actionEditUndo, &QAction::triggered, [&](bool)
	{
		onEditUndo();
	});

	connect(ui->actionEditRedo, &QAction::triggered, [&](bool)
	{
		onEditRedo();
	});

	connect(ui->actionEditCut, &QAction::triggered, [&](bool)
	{
		onEditCut();
	});

	connect(ui->actionEditCopy, &QAction::triggered, [&](bool)
	{
		onEditCopy();
	});

	connect(ui->actionEditPaste, &QAction::triggered, [&](bool)
	{
		onEditPaste();
	});

	connect(ui->actionEditFieldsReturnValue, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::ReturnValue, checked);
	});

	connect(ui->actionEditFieldsAuthor, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Author, checked);
	});

	connect(ui->actionEditFieldsErrors, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Errors, checked);
	});

	connect(ui->actionEditFieldsAttributes, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Attributes, checked);
	});

	connect(ui->actionEditFieldsConformingTo, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::ConformingTo, checked);
	});

	connect(ui->actionEditFieldsNotes, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Notes, checked);
	});

	connect(ui->actionEditFieldsBugs, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Bugs, checked);
	});

	connect(ui->actionEditFieldsReportingBugs, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::ReportingBugs, checked);
	});

	connect(ui->actionEditFieldsSeeAlso, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::SeeAlso, checked);
	});

	connect(ui->actionEditFieldsColophon, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Colophon, checked);
	});

	connect(ui->actionEditFieldsCopyright, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Copyright, checked);
	});

	connect(ui->actionEditFieldsKeywords, &QAction::triggered, [&](bool checked)
	{
		onEditFields(Field::Keywords, checked);
	});

	connect(ui->actionEditPreferences, &QAction::triggered, [&](bool)
	{
		onEditPreferences();
	});
}

void MainWindow::prepareInsertMenuConnections()
{
	connect(ui->actionInsertTable, &QAction::triggered, [&](bool)
	{
		onInsertTable();
	});

	connect(ui->actionInsertHyperlink, &QAction::triggered, [&](bool checked)
	{
		onInsertHyperlink(checked);
	});

	connect(ui->actionInsertListBulleted, &QAction::triggered, [&](bool)
	{
		onInsertListBulleted();
	});

	connect(ui->actionInsertListNumbered, &QAction::triggered, [&](bool)
	{
		onInsertListNumbered();
	});
}

void MainWindow::prepareFormatMenuConnections()
{
	connect(ui->actionFormatTextBold, &QAction::triggered, [&](bool checked)
	{
		onFormatTextBold(checked);
	});

	connect(ui->actionFormatTextItalic, &QAction::triggered, [&](bool checked)
	{
		onFormatTextItalic(checked);
	});

	connect(ui->actionFormatTextUnderline, &QAction::triggered, [&](bool checked)
	{
		onFormatTextUnderline(checked);
	});

	connect(ui->actionFormatTextStrikethough, &QAction::triggered, [&](bool checked)
	{
		onFormatTextStrikeThrough(checked);
	});

	connect(ui->actionFormatTextRemoveFormatting, &QAction::triggered, [&]()
	{
		onFormatTextRemoveFormatting();
	});

	connect(ui->actionFormatColorText, &QAction::triggered, [&]()
	{
		onFormatColorText();
	});

	connect(ui->actionFormatColorHighlight, &QAction::triggered, [&]()
	{
		onFormatColorHighLight();
	});

	connect(ui->actionFormatParagraphLeftAligned, &QAction::triggered, [&]()
	{
		onFormatParagraphLeftAligned();
	});

	connect(ui->actionFormatParagraphRightAligned, &QAction::triggered, [&]()
	{
		onFormatParagraphRightAligned();
	});

	connect(ui->actionFormatParagraphLeftToRight, &QAction::triggered, [&]()
	{
		onFormatParagraphLeftToRight();
	});

	connect(ui->actionFormatParagraphRightToLeft, &QAction::triggered, [&]()
	{
		onFormatParagraphRightToLeft();
	});
}

void MainWindow::prepareViewMenuConnections()
{
	connect(ui->actionViewFullScreen, &QAction::triggered, [&](bool checked)
	{
		onViewFullScreen(checked);
	});

	connect(ui->actionViewMenuBar, &QAction::triggered, [&](bool checked)
	{
		onViewMenuBar(checked);
	});

	connect(ui->actionViewToolBar, &QAction::triggered, [&](bool checked)
	{
		onViewToolBar(checked);
	});
}

void MainWindow::prepareWindowMenuConnections()
{
	connect(ui->actionWindowTabbedView, &QAction::triggered,
					this, &MainWindow::onWindowTabbedView);
	connect(ui->actionWindowTile, &QAction::triggered,
					this, &MainWindow::onWindowTile);
	connect(ui->actionWindowTileHorizontally, &QAction::triggered,
					this, &MainWindow::onWindowTileHorizontally);
	connect(ui->actionWindowTileVertically, &QAction::triggered,
					this, &MainWindow::onWindowTileVertically);
	connect(ui->actionWindowCascade, &QAction::triggered,
					this, &MainWindow::onWindowCascade);
}

void MainWindow::prepareHelpMenuConnections()
{
	connect(ui->actionHelpAbout, &QAction::triggered, [&](bool)
	{
		onHelpAbout();
	});

	connect(ui->actionHelpAboutQt, &QAction::triggered, [&](bool)
	{
		onHelpAboutQt();
	});
}

void MainWindow::prepareToolBarToolButtonConnections()
{
	connect(m_tblMakerWid, &TableMakerWidget::tableSizeSelected,
					this, &MainWindow::insertTable);

	connect(m_txtClrPickWid, &ColorPickerWidget::colorSelected,
					[&](const QColor& color)
	{
		m_currTxtColor	= color;

		m_txtClrToolBtn->setIcon(textColorIcon(color));
		m_txtClrToolBtn->menu()->close();
		applyTextColor(color);
	});

	connect(m_txtClrPickWid, &ColorPickerWidget::automaticSelected, [&]()
	{
		m_currTxtColor	= palette().color(QPalette::Text);

		m_txtClrToolBtn->setIcon(textAutoColorIcon());
		m_txtClrToolBtn->menu()->close();
		applyAutomaticTextColor();
	});

	connect(m_txtClrToolBtn, &QToolButton::clicked, [&](bool)
	{
		applyTextColor(m_currTxtColor);
	});

	connect(m_hltClrPickWid, &ColorPickerWidget::colorSelected,
					[&](const QColor& color)
	{
		m_currHltColor	= color;

		m_hltClrToolBtn->setIcon(highLightColorIcon(color));
		m_hltClrToolBtn->menu()->close();
		applyHighLightColor(color);
	});

	connect(m_hltClrPickWid, &ColorPickerWidget::automaticSelected, [&]()
	{
		m_currHltColor	= palette().color(QPalette::Base);

		m_hltClrToolBtn->setIcon(highLightAutoColorIcon());
		m_hltClrToolBtn->menu()->close();
		applyAutomaticHighLightColor();
	});

	connect(m_hltClrToolBtn, &QToolButton::clicked, [&](bool)
	{
		applyHighLightColor(m_currHltColor);
	});
}

void MainWindow::prepareConnections()
{
	connect(m_mdiArea, &QMdiArea::subWindowActivated,
					this, &MainWindow::updateMenus);
	connect(m_mdiArea, &QMdiArea::subWindowActivated,
					this, QOverload<>::of(&MainWindow::updateWindowTitle));

	prepareFileMenuConnections();
	prepareEditMenuConnections();
	prepareInsertMenuConnections();
	prepareFormatMenuConnections();
	prepareViewMenuConnections();
	prepareWindowMenuConnections();
	prepareHelpMenuConnections();
	prepareToolBarToolButtonConnections();
}
