#include <gui/cpw/colorpickerwidget.hpp>

#include <QApplication>
#include <QStyle>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QFrame>
#include <QComboBox>
#include <QVariant>
#include <QToolButton>
#include <QColorDialog>
#include <gui/cpw/colorwidget.hpp>

#include <QDebug>

#define CPW_PROP_IS_RECENT	"is_recent"

const int	ColorPickerWidget::ROWS	= 12;
const int	ColorPickerWidget::COLS	= 12;

ColorPickerWidget::ColorPickerWidget(QWidget *parent) : QWidget(parent)
{
	m_vboxLayout	= new QVBoxLayout();
	m_gridLayout	= new QGridLayout();

	setLayout(m_vboxLayout);

	m_titleLabel	= new QLabel();
	connect(this, &ColorPickerWidget::windowTitleChanged, [&](const QString& title)
	{
		m_titleLabel->setText(QString("<b>%1</b>").arg(title));
	});
	m_vboxLayout->addWidget(m_titleLabel);

	m_autoColorToolBtn	= new QToolButton();
	m_autoColorToolBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	m_autoColorToolBtn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	m_autoColorToolBtn->setIcon(QIcon("://img/automatic.png"));
	connect(m_autoColorToolBtn, &QToolButton::clicked, [&](bool)
	{
		emit automaticSelected();
	});
	m_vboxLayout->addWidget(m_autoColorToolBtn);

	m_hLine0	= new QFrame();
	m_hLine0->setFrameShape(QFrame::HLine);
	m_vboxLayout->addWidget(m_hLine0);

	m_colorTypeComboBox	= new QComboBox();
	m_colorTypeComboBox->addItem(tr("Standard"),
															 QVariant::fromValue<ColorType>(ColorType::Standard));
	m_colorTypeComboBox->addItem(tr("Custom"),
															 QVariant::fromValue<ColorType>(ColorType::Custom));
	m_colorTypeComboBox->addItem(tr("HTML"),
															 QVariant::fromValue<ColorType>(ColorType::HTML));
	m_colorTypeComboBox->addItem(tr("Tonal"),
															 QVariant::fromValue<ColorType>(ColorType::Tonal));
	connect(m_colorTypeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
					[&](int index)
	{
		switch (m_colorTypeComboBox->itemData(index).value<ColorType>())
		{
		case ColorType::Custom:
			fillCustom();

			break;

		case ColorType::HTML:
			fillHtml();

			break;

		case ColorType::Tonal:
			fillTonal();

			break;

		case ColorType::Standard:
			fillStandard();

			break;
		}
	});
	m_vboxLayout->addWidget(m_colorTypeComboBox);

	m_gridLayout	= new QGridLayout();
	m_gridLayout->setSpacing(0);
	m_gridLayout->setMargin(0);

	m_vboxLayout->addLayout(m_gridLayout);

	ColorWidget	*cw	{ nullptr };
	for (int r=0; r<ROWS; ++r)
	{
		for (int c=0; c<COLS; ++c)
		{
			cw	= new ColorWidget();
			connect(cw, &ColorWidget::colorSelected,
							this, &ColorPickerWidget::onColorSelected);
			cw->setFixedSize(18, 18);
			cw->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
			m_gridLayout->addWidget(cw, r, c);
		}
	}

	m_hLine1	= new QFrame();
	m_hLine1->setFrameShape(QFrame::HLine);
	m_vboxLayout->addWidget(m_hLine1);

	m_recentLabel	= new QLabel(tr("Recent"));
	m_vboxLayout->addWidget(m_recentLabel);

	m_hboxLayout	= new QHBoxLayout();
	m_hboxLayout->setSpacing(0);
	m_hboxLayout->setMargin(0);
	for (int i=0; i<COLS; ++i)
	{
		cw	= new ColorWidget();
		cw->setProperty(CPW_PROP_IS_RECENT, true);
		connect(cw, &ColorWidget::colorSelected,
						this, &ColorPickerWidget::onColorSelected);
		cw->setFixedSize(18, 18);
		cw->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		m_hboxLayout->addWidget(cw);
	}
	m_vboxLayout->addLayout(m_hboxLayout);

	m_hLine2	= new QFrame();
	m_hLine2->setFrameShape(QFrame::HLine);
	m_vboxLayout->addWidget(m_hLine2);

	m_customColorToolBtn	= new QToolButton();
	m_customColorToolBtn->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	m_customColorToolBtn->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	m_customColorToolBtn->setIcon(QIcon("://img/color-wheel.png"));
	m_customColorToolBtn->setText(tr("Custom Color..."));
	m_vboxLayout->addWidget(m_customColorToolBtn);
	connect(m_customColorToolBtn, &QToolButton::clicked, [&](bool)
	{
		QColorDialog	dlg;

		dlg.setOption(QColorDialog::DontUseNativeDialog);

		if (dlg.exec() == QDialog::Accepted)
		{
			ColorProperties	c	{ rgbColorText(dlg.selectedColor()),
													dlg.selectedColor() };

			addCustomColor(c);
			addRecentColor(c);

			emit colorSelected(c.color);
		}
	});

	fillStandard();
	fillRecent();
}

void ColorPickerWidget::setAutoColorText(const QString& text)
{
	m_autoColorToolBtn->setText(text);
}

void ColorPickerWidget::setCustomColors(const QList<ColorProperties>& colors)
{
	if (colors.size() > ROWS * COLS)
	{
		m_customColors	= colors.mid(0, ROWS * COLS);
	}
	else
	{
		m_customColors	= colors;
	}

	if (m_colorTypeComboBox->currentData().value<ColorType>() == ColorType::Custom)
	{
		fillCustom();
	}
}

void ColorPickerWidget::addCustomColor(const ColorProperties& color)
{
	m_customColors.push_back(color);

	if (m_customColors.size() > ROWS * COLS)
	{
		m_customColors.removeAt(0);
	}

	if (m_colorTypeComboBox->currentData().value<ColorType>() == ColorType::Custom)
	{
		fillCustom();
	}
}

void ColorPickerWidget::setRecentColors(const QList<ColorProperties>& colors)
{
	if (colors.size() > COLS)
	{
		m_recentColors	= colors.mid(0, COLS);
	}
	else
	{
		m_recentColors	= colors;
	}

	fillRecent();
}

void ColorPickerWidget::addRecentColor(const ColorProperties& color)
{
	m_recentColors.push_back(color);

	if (m_recentColors.size() > COLS)
	{
		m_recentColors.removeAt(0);
	}

	fillRecent();
}

void ColorPickerWidget::fillCustom()
{
	ColorWidget	*cw	{ };
	size_t			n		{ };

	for (int r=0; r<ROWS; ++r)
	{
		for (int c=0; c<COLS; ++c)
		{
			cw	= qobject_cast<ColorWidget*>(m_gridLayout->itemAtPosition(r, c)->widget());
			if (n == static_cast<size_t>(m_customColors.size()))
			{
				cw->setEnabled(false);
				cw->setToolTip("");
			}
			else
			{
				cw->setEnabled(true);
				cw->setColor(m_customColors[n].color);
				cw->setToolTip(m_customColors[n].name);
				++n;
			}
		}
	}
}

void ColorPickerWidget::fillHtml()
{
	ColorWidget	*cw	{ };
	size_t			n		{ };

	for (int r=0; r<ROWS; ++r)
	{
		for (int c=0; c<COLS; ++c)
		{
			cw	= qobject_cast<ColorWidget*>(m_gridLayout->itemAtPosition(r, c)->widget());
			if (n == g_numHtmlColors)
			{
				cw->setEnabled(false);
				cw->setToolTip("");
			}
			else
			{
				cw->setEnabled(true);
				cw->setColor(g_htmlColors[n].color);
				cw->setToolTip(g_htmlColors[n].name);
				++n;
			}
		}
	}
}

void ColorPickerWidget::fillTonal()
{
	ColorWidget	*cw	{ };
	size_t			n		{ };

	for (int r=0; r<ROWS; ++r)
	{
		for (int c=0; c<COLS; ++c)
		{
			cw	= qobject_cast<ColorWidget*>(m_gridLayout->itemAtPosition(r, c)->widget());
			if (n == g_numTonalColors)
			{
				cw->setEnabled(false);
				cw->setToolTip("");
			}
			else
			{
				cw->setEnabled(true);
				cw->setColor(g_tonalColors[n].color);
				cw->setToolTip(g_tonalColors[n].name);
				++n;
			}
		}
	}
}

void ColorPickerWidget::fillStandard()
{
	ColorWidget	*cw	{ };
	size_t			n		{ };

	for (int r=0; r<ROWS; ++r)
	{
		for (int c=0; c<COLS; ++c)
		{
			cw	= qobject_cast<ColorWidget*>(m_gridLayout->itemAtPosition(r, c)->widget());
			if (n == g_numStandardColors)
			{
				cw->setEnabled(false);
				cw->setToolTip("");
			}
			else
			{
				cw->setEnabled(true);
				cw->setColor(g_standardColors[n].color);
				cw->setToolTip(g_standardColors[n].name);
				++n;
			}
		}
	}
}

void ColorPickerWidget::fillRecent()
{
	ColorWidget	*cw	{ };
	size_t			n		{ };

	for (int c=0; c<COLS; ++c)
	{
		cw	= qobject_cast<ColorWidget*>(m_hboxLayout->itemAt(c)->widget());
		if (n == static_cast<size_t>(m_recentColors.size()))
		{
			cw->setEnabled(false);
			cw->setToolTip("");
		}
		else
		{
			cw->setEnabled(true);
			cw->setColor(m_recentColors[n].color);
			cw->setToolTip(m_recentColors[n].name);
			++n;
		}
	}
}

QString ColorPickerWidget::rgbColorText(QColor c)
{
	return QString("RGB(%1, %2, %3)")
			.arg(c.red())
			.arg(c.green())
			.arg(c.blue());
}

void ColorPickerWidget::onColorSelected(ColorWidget* w, const QColor& color)
{
	if (w->property(CPW_PROP_IS_RECENT).isNull())
	{
		addRecentColor(ColorProperties {w->toolTip(), color });
	}

	emit colorSelected(color);
}
