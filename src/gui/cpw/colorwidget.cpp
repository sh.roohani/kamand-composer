#include <gui/cpw/colorwidget.hpp>

#include <QPainter>
#include <QPen>
#include <QMouseEvent>

ColorWidget::ColorWidget(QWidget *parent) :
	ColorWidget(Qt::white, parent)
{
	setAttribute(Qt::WA_Hover, true);
}

ColorWidget::ColorWidget(const QColor& color, QWidget *parent) :
	QWidget(parent),
	m_color(color)
{

}

void ColorWidget::setColor(const QColor &color)
{
	m_color	= color;

	update();
}

void ColorWidget::paintEvent(QPaintEvent* /*e*/)
{
	QPainter	p(this);

	p.fillRect(rect(), QColor(247, 247, 247));
	if (isEnabled())
	{
		if (m_enter)
		{
			QPen	pen(Qt::black);
			pen.setWidth(1);
			p.setPen(pen);
			p.drawRect(rect().adjusted(0, 0, -1, -1));
		}
		p.fillRect(rect().adjusted(3, 3, -3, -3), m_color);
	}
}

void ColorWidget::enterEvent(QEvent* /*e*/)
{
	m_enter	= true;

	update();
}

void ColorWidget::leaveEvent(QEvent* /*e*/)
{
	m_enter	= false;

	update();
}

void ColorWidget::mousePressEvent(QMouseEvent* e)
{
	if (e->button() == Qt::LeftButton)
	{
		m_pressed	= true;
	}
}

void ColorWidget::mouseReleaseEvent(QMouseEvent* e)
{
	if (e->button() == Qt::LeftButton)
	{
		m_pressed	= false;

		emit colorSelected(this, m_color);
	}
}
