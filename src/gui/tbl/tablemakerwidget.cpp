#include <gui/tbl/tablemakerwidget.hpp>

#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>

#include <gui/tbl/tablewidget.hpp>
#include <gui/tbl/tablepropsdialog.hpp>

TableMakerWidget::TableMakerWidget(QWidget *parent) : QWidget(parent)
{
	m_vboxLayout	= new QVBoxLayout();

	setLayout(m_vboxLayout);

	m_titleLabel	= new QLabel(tr("<b>Table</b>"));
	m_tableWidget	= new TableWidget();
	connect(m_tableWidget, &TableWidget::tableSizeSelected,
					this, &TableMakerWidget::tableSizeSelected);
	m_moreOptsBtn	= new QPushButton(tr("More Options..."));
	connect(m_moreOptsBtn, &QPushButton::clicked, [&](bool)
	{
		TablePropsDialog	dlg;

		if (dlg.exec() == QDialog::Accepted)
		{
			dlg.collectUi();

			emit tableSizeSelected(dlg.m_hCells, dlg.m_vCells);
		}
	});

	m_vboxLayout->addWidget(m_titleLabel);
	m_vboxLayout->addWidget(m_tableWidget);
	m_vboxLayout->addWidget(m_moreOptsBtn);
}
