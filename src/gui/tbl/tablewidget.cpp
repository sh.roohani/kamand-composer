#include <gui/tbl/tablewidget.hpp>

#include <QPainter>
#include <QMouseEvent>
#include <QToolTip>

const int TableWidget::CELL_SIZE	= 24;
const int TableWidget::HORZ_CELLS	= 10;
const int TableWidget::VERT_CELLS	= 15;

#include <QDebug>

TableWidget::TableWidget(QWidget *parent) : QWidget(parent)
{
	setMouseTracking(true);
}

QSize TableWidget::sizeHint() const
{
	return QSize(HORZ_CELLS * CELL_SIZE, VERT_CELLS * CELL_SIZE);
}

void TableWidget::paintEvent(QPaintEvent* e)
{
	QPainter	p(this);
	QRect			rc(rect());

	QWidget::paintEvent(e);

	p.fillRect(0, 0, m_activeHCells * CELL_SIZE, m_activeVCells * CELL_SIZE, Qt::lightGray);

	for (int i=0; i<HORZ_CELLS; ++i)
	{
		p.drawLine(i * CELL_SIZE, 0, i * CELL_SIZE, rc.bottom() - 1);
	}

	p.drawLine(rc.right() - 1, 0, rc.right() - 1, rc.bottom() - 1);

	for (int i=0; i<VERT_CELLS; ++i)
	{
		p.drawLine(0, i * CELL_SIZE, rc.right() - 1, i * CELL_SIZE);
	}

	p.drawLine(0, rc.bottom() - 1,  rc.right() - 1, rc.bottom() - 1);
}

void TableWidget::mouseMoveEvent(QMouseEvent* e)
{
	QPoint	pos			= e->pos();

	m_activeHCells	= pos.x() / CELL_SIZE + (pos.x() % CELL_SIZE ? 1 : 0),
	m_activeVCells	= pos.y() / CELL_SIZE + (pos.y() % CELL_SIZE ? 1 : 0);

	update();

	QToolTip::showText(e->globalPos(),
										 QString("%1 \u00d7 %2")
										 .arg(m_activeHCells)
										 .arg(m_activeVCells));

	QWidget::mouseMoveEvent(e);
}

void TableWidget::mousePressEvent(QMouseEvent* e)
{
	m_mousePressed	= true;

	QWidget::mousePressEvent(e);
}

void TableWidget::mouseReleaseEvent(QMouseEvent *e)
{
	if (m_mousePressed)
	{
		m_mousePressed	= false;

		emit tableSizeSelected(m_activeHCells, m_activeVCells);
	}

	QWidget::mouseReleaseEvent(e);
}

void TableWidget::leaveEvent(QEvent* e)
{
	m_activeHCells	= m_activeVCells	= 0;

	QToolTip::hideText();

	update();

	QWidget::leaveEvent(e);
}
