#include <gui/tbl/tablepropsdialog.hpp>
#include "ui_tablepropsdialog.h"

TablePropsDialog::TablePropsDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::TablePropsDialog)
{
	ui->setupUi(this);
}

TablePropsDialog::~TablePropsDialog()
{
	delete ui;
}

void TablePropsDialog::populateUi()
{
	ui->columnsSpinBox->setValue(m_hCells);
	ui->rowsSpinBox->setValue(m_vCells);
}

void TablePropsDialog::collectUi()
{
	m_hCells	= ui->columnsSpinBox->value();
	m_vCells	= ui->rowsSpinBox->value();

}
