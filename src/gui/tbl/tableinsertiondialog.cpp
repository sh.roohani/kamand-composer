#include <gui/tbl/tableinsertiondialog.hpp>
#include "ui_tableinsertiondialog.h"

TableInsertionDialog::TableInsertionDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::TableInsertionDialog)
{
	ui->setupUi(this);
}

TableInsertionDialog::~TableInsertionDialog()
{
	delete ui;
}

void TableInsertionDialog::populateUi()
{
	ui->numberSpinBox->setValue(m_number);
	m_before ? ui->beforeRadioButton->setChecked(true) :
						 ui->AfterRadioButton->setChecked(true);
}

void TableInsertionDialog::collectUi()
{
	m_number	= ui->numberSpinBox->value();
	m_before	= ui->beforeRadioButton->isChecked();
}
