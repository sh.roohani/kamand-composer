#include <gui/prefs/prefsuiwidget.hpp>
#include "ui_prefsuiwidget.h"

#include <kc_global_vars.hpp>

#include <QFontDialog>
#include <QTextCharFormat>
#include <QTextCursor>

PrefsUiWidget::PrefsUiWidget(QWidget *parent) :
	PrefsBaseWidget(parent),
	ui(new Ui::PrefsUiWidget)
{
  ui->setupUi(this);

	connect(ui->englishFontBrowserToolButton, &QToolButton::clicked, [&](bool)
	{
		chooseFont(Language::English);
	});

	connect(ui->persianFontBrowserToolButton, &QToolButton::clicked, [&](bool)
	{
		chooseFont(Language::Persian);
	});
}

PrefsUiWidget::~PrefsUiWidget()
{
  delete ui;
}

void PrefsUiWidget::populateUi(Config* cfg)
{
	if (!m_enMlteFont.fromString(cfg->m_enMlteFont))
	{
		m_enMlteFont	= ui->englishSampleTextEdit->font().toString();
	}

	setFontInfo(m_enMlteFont, ui->englishFontLabel, ui->englishSampleTextEdit);

	if (!m_faMlteFont.fromString(cfg->m_faMlteFont))
	{
		m_faMlteFont	= ui->persianSampleTextEdit->font().toString();
	}

	setFontInfo(m_faMlteFont, ui->persianFontLabel, ui->persianSampleTextEdit);
}

void PrefsUiWidget::collectUi(Config* cfg)
{
	cfg->m_enMlteFont	= m_enMlteFont.toString();
	cfg->m_faMlteFont	= m_faMlteFont.toString();
}

bool PrefsUiWidget::browseFont(QFont &f)
{
	QFontDialog	dlg;

	dlg.setOption(QFontDialog::DontUseNativeDialog);
	dlg.setCurrentFont(f);

	if (dlg.exec() == QDialog::Accepted)
	{
		f	= dlg.selectedFont();

		f.setBold(false);
		f.setItalic(false);
		f.setUnderline(false);
		f.setStrikeOut(false);

		return true;
	}

	return false;
}

void PrefsUiWidget::setFontInfo(const QFont& fnt, QLabel *lbl, QTextEdit *te)
{
	QTextCharFormat	f	= te->currentCharFormat();
	QTextCursor			c	= te->textCursor();

	f.setFont(fnt);
	te->selectAll();
	te->setCurrentCharFormat(f);
	c.movePosition(QTextCursor::End);
	te->setTextCursor(c);
	lbl->setText(fnt.family());
}

void PrefsUiWidget::chooseFont(Language l)
{
	switch (l)
	{
	case Language::English:
		if (browseFont(m_enMlteFont))
		{
			setFontInfo(m_enMlteFont, ui->englishFontLabel, ui->englishSampleTextEdit);
		}

		break;

	case Language::Persian:
		if (browseFont(m_faMlteFont))
		{
			setFontInfo(m_faMlteFont, ui->persianFontLabel, ui->persianSampleTextEdit);
		}

		break;

	default:

		break;
	}
}
