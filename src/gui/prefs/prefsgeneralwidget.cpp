#include <gui/prefs/prefsgeneralwidget.hpp>
#include "ui_prefsgeneralwidget.h"

#include <kc_config.hpp>

PrefsGeneralWidget::PrefsGeneralWidget(QWidget *parent) :
	PrefsBaseWidget(parent),
	ui(new Ui::PrefsGeneralWidget)
{
  ui->setupUi(this);
}

PrefsGeneralWidget::~PrefsGeneralWidget()
{
  delete ui;
}

void PrefsGeneralWidget::populateUi(Config* cfg)
{
  ui->exitConfirmationCheckBox->setChecked(cfg->m_exitConfirmation);
}

void PrefsGeneralWidget::collectUi(Config* cfg)
{
  cfg->m_exitConfirmation = ui->exitConfirmationCheckBox->isChecked();
}
