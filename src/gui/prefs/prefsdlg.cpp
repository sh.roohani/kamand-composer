#include <gui/prefs/prefsdlg.hpp>
#include "ui_prefsdlg.h"

#include <QListWidgetItem>
#include <QIcon>

#include <kc_config.hpp>

#include <gui/prefs/prefsbasewidget.hpp>
#include <gui/prefs/prefsgeneralwidget.hpp>
#include <gui/prefs/prefsuiwidget.hpp>

#include <QDebug>

PrefsDlg::PrefsDlg(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::PrefsDlg)
{
  ui->setupUi(this);

  m_title = windowTitle();

  QWidget *w  = nullptr;
  while((w = ui->stackedWidget->widget(0)))
  {
    ui->stackedWidget->removeWidget(w);
    w->deleteLater();
  }

  ui->listWidget->setWordWrap(true);

	QListWidgetItem	*item = nullptr;
	PrefsBaseWidget	*pbw  = nullptr;

	pbw = new PrefsGeneralWidget();
	ui->stackedWidget->addWidget(pbw);
	item  = new QListWidgetItem(QIcon(":/img/preferences-general.png"),
															pbw->windowTitle());
  ui->listWidget->addItem(item);

	pbw = new PrefsUiWidget();
	ui->stackedWidget->addWidget(pbw);
	item  = new QListWidgetItem(QIcon(":/img/preferences-ui.png"),
															pbw->windowTitle());
  ui->listWidget->addItem(item);

	connect(ui->listWidget, &QListWidget::currentRowChanged, [&](int index)
  {
    ui->stackedWidget->setCurrentIndex(index);

    setWindowTitle(m_title + " - " +
                   ui->stackedWidget->currentWidget()->windowTitle());
  });
}

PrefsDlg::~PrefsDlg()
{
  delete ui;
}

void PrefsDlg::populateUi(Config* cfg)
{
	PrefsBaseWidget  *w  = nullptr;

  for (int i=0; i<ui->stackedWidget->count(); ++i)
  {
		if ((w = qobject_cast<PrefsBaseWidget*>(ui->stackedWidget->widget(i))))
    {
			w->populateUi(cfg);
    }
  }
}

void PrefsDlg::collectUi(Config* cfg)
{
	PrefsBaseWidget  *w  = nullptr;

  for (int i=0; i<ui->stackedWidget->count(); ++i)
  {
		if ((w = qobject_cast<PrefsBaseWidget*>(ui->stackedWidget->widget(i))))
    {
			w->collectUi(cfg);
    }
  }
}
