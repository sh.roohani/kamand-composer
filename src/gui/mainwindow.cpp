#include <gui/mainwindow.hpp>
#include "ui_mainwindow.h"

#include <QMdiArea>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QPalette>
#include <gui/mdichild.hpp>

#include <gui/cpw/colorpickerwidget.hpp>

#include <kc_global_vars.hpp>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	m_mdiArea(new QMdiArea())
{
	ui->setupUi(this);

	m_currHltColor	= palette().color(QPalette::Base);
	m_currTxtColor	= palette().color(QPalette::Text);

	m_mdiArea->setActivationOrder(QMdiArea::CreationOrder);
	m_mdiArea->setTabsClosable(true);
	m_mdiArea->setTabsMovable(true);
	m_mdiArea->setTabShape(QTabWidget::Triangular);
	setCentralWidget(m_mdiArea);


	prepareActions();
	prepareMenus();
	prepareToolBars();
	prepareConnections();

	updateMenus();
	updateToolBars();

	applyMlteFonts();
}


MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::loadConfigurationParameters()
{
	restoreGeometry(g_cfg->m_mwGeometry);
	restoreState(g_cfg->m_mwState);
	if (g_cfg->m_fullscreen)
	{
		showFullScreen();
	}
	else
	{
		show();
	}

	m_txtClrPickWid->setCustomColors(g_cfg->m_txtCustomColors);
	m_txtClrPickWid->setRecentColors(g_cfg->m_txtRecentColors);
	m_hltClrPickWid->setCustomColors(g_cfg->m_hltCustomColors);
	m_hltClrPickWid->setRecentColors(g_cfg->m_hltRecentColors);

	m_mdiArea->setViewMode(g_cfg->m_tabbedView ? QMdiArea::TabbedView :
																							 QMdiArea::SubWindowView);
	ui->actionWindowTabbedView->setChecked(g_cfg->m_tabbedView);

	ui->actionViewMenuBar->setChecked(true);
	ui->actionViewToolBar->setChecked(true);
}

void MainWindow::saveConfigurationParameters()
{
	g_cfg->m_mwGeometry	= saveGeometry();
	g_cfg->m_mwState		= saveState();
	g_cfg->m_fullscreen	= isFullScreen();

	g_cfg->m_txtCustomColors	= m_txtClrPickWid->customColors();
	g_cfg->m_txtRecentColors	= m_txtClrPickWid->recentColors();
	g_cfg->m_hltCustomColors	= m_hltClrPickWid->customColors();
	g_cfg->m_hltRecentColors	= m_hltClrPickWid->recentColors();

	g_cfg->m_tabbedView	= m_mdiArea->viewMode() == QMdiArea::TabbedView;
}

MdiChild* MainWindow::newMdiChild()
{
	MdiChild			*mdiChild			{ new MdiChild() };
	QMdiSubWindow	*mdiSubWindow	{ m_mdiArea->addSubWindow(mdiChild) };

	mdiChild->setAttribute(Qt::WA_DeleteOnClose);

	mdiSubWindow->setWindowIcon(QIcon("://img/kamand.png"));
	mdiChild->setMdiSubWindow(mdiSubWindow);
	mdiSubWindow->resize(mdiChild->size());

	connect(mdiChild, &MdiChild::modified, [=](Language language, Field field)
	{
		onMdiChildModified(mdiChild, language, field);
	});

	connect(mdiChild, &MdiChild::currentCharFormatChanged,
					this, &MainWindow::onCurrentTextFormatChanged);
	connect(mdiChild, &MdiChild::currentFocusChanged,
					this, &MainWindow::onCurrentFocusChanged);

	return mdiChild;
}

MdiChild *MainWindow::currentMdiChild()
{
	MdiChild			*mdiChild		{ nullptr };
	QMdiSubWindow	*subWindow	{ m_mdiArea->currentSubWindow() };

	if (subWindow)
	{
		mdiChild	= qobject_cast<MdiChild*>(subWindow->widget());
	}

	return mdiChild;
}

QList<MdiChild*> MainWindow::mdiChildList()
{
	QList<QMdiSubWindow*>	subWindows	{ m_mdiArea->subWindowList() };
	QList<MdiChild*>			mdiChildren;

	for (const auto sw: subWindows)
	{
		mdiChildren << qobject_cast<MdiChild*>(sw->widget());
	}

	return mdiChildren;
}

void MainWindow::applyMlteFonts()
{
	QList<MdiChild*>	mdiChildren	= m_mdiArea->findChildren<MdiChild*>();
	QFont							f;
	for (auto c: mdiChildren)
	{
		f.fromString(g_cfg->m_enMlteFont);
		c->setMultilineTextEditorsFont(Language::English, f);
		f.fromString(g_cfg->m_faMlteFont);
		c->setMultilineTextEditorsFont(Language::Persian, f);
	}
}

void MainWindow::updateWindowTitle(const QString& postfix)
{
	setWindowTitle(QString(KC_APP_NAME "%1")
								 .arg(postfix.isEmpty() ? "" : QString(" - %1").arg(postfix)));
}

void MainWindow::updateEditFieldsMenu(MdiChild* mdiChild)
{
	QList<Field>		fields;
	QList<QAction*>	acts		= ui->menuEditFields->actions();
	int							i				= 0;

	if (mdiChild)
	{
		fields	= mdiChild->fields();
	}

	for (Field f=Field::ReturnValue; f<Field::Last; ++f, ++i)
	{
		acts[i]->setChecked(fields.contains(f));
		acts[i]->setEnabled(mdiChild);
	}
}

void MainWindow::getDocumentSaveFileNames(QWidget* parent, MdiChild* mdiChild,
																					const QString& caption,
																					QString& enName, QString& faName)
{
	QString	fName	{ QFileDialog::getExistingDirectory(parent, caption,
																										g_cfg->m_lastDocDir) };

	if (!fName.isEmpty())
	{
		if (!fName.endsWith(QDir::separator()))
		{
			fName.append(QDir::separator());
		}

		fName.append(mdiChild->name());

		enName	= fName + "-en" KC_EXT_DOCUMENT;
		faName	= fName + "-fa" KC_EXT_DOCUMENT;
	}
}

QString MainWindow::getTemplateSaveFileName(QWidget* parent,
																						const QString& caption)
{
	QString	fName,
					selectedFilter,
					filter	{ KC_FILTER_TEMPLATES ";;" KC_FILTER_ALL_FILES };

	fName	= QFileDialog::getSaveFileName(parent, caption, g_cfg->m_lastTmplDir,
																			 filter, &selectedFilter);

	if (selectedFilter == filter.split(";;")[0] &&
			!fName.endsWith(KC_EXT_TEMPLATE))
	{
		fName.append(KC_EXT_TEMPLATE);
	}

	return fName;
}

bool MainWindow::fileExists(const QString &fileName)
{
	return QFileInfo::exists(fileName);
}

void MainWindow::saveDocument(const QString& docName, const QByteArray& doc)
{
	QFile	file(docName);

	file.open(QIODevice::WriteOnly);
	file.write(doc);
	file.close();
}

void MainWindow::updateWindowTitle()
{
	MdiChild	*mdiChild	{  currentMdiChild() };
	QString		postfix;

	if (mdiChild)
	{
		postfix	= mdiChild->title();
	}

	updateWindowTitle(postfix);
}
