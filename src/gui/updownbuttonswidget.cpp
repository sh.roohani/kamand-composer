#include <gui/updownbuttonswidget.hpp>

#include <QVBoxLayout>
#include <QSpacerItem>
#include <QToolButton>
#include <QStyle>

UpDownButtonsWidget::UpDownButtonsWidget(QWidget *parent) :
	UpDownButtonsWidget(-1, parent)
{
}

UpDownButtonsWidget::UpDownButtonsWidget(int rowNumber, QWidget *parent) :
	QWidget(parent),
	m_rowNumber(rowNumber)
{
	prepare();
}

void UpDownButtonsWidget::setRownNumber(int rowNumber)
{
	if (m_rowNumber > -1)
	{
		m_rowNumber	= rowNumber;
	}
}

void UpDownButtonsWidget::setUpEnabled(bool enabled)
{
	m_upButton->setEnabled(enabled);
}

bool UpDownButtonsWidget::isUpEnabled() const
{
	return m_upButton->isEnabled();
}

void UpDownButtonsWidget::setDownEnabled(bool enabled)
{
	m_downButton->setEnabled(enabled);
}

bool UpDownButtonsWidget::isDownEnabled() const
{
	return m_downButton->isEnabled();
}

void UpDownButtonsWidget::prepare()
{
	m_vboxLayout	= new QVBoxLayout();

	m_vboxLayout->setContentsMargins(0, 0, 0, 0);
	m_vboxLayout->setMargin(0);
	m_vboxLayout->setSpacing(0);
	setLayout(m_vboxLayout);

	m_vboxLayout->addItem(new QSpacerItem(0, 1,
																				QSizePolicy::Minimum,
																				QSizePolicy::Expanding));

	m_upButton			= new QToolButton();
	m_upButton->setFocusPolicy(Qt::NoFocus);
//	m_upButton->setIcon(style()->standardIcon(QStyle::SP_ArrowUp));
	m_upButton->setIcon(QIcon("://img/up.png"));
	m_upButton->setToolTip(tr("Move Up"));
	m_vboxLayout->addWidget(m_upButton);
	connect(m_upButton, &QToolButton::clicked, [&](bool)
	{
		emit upClicked(m_rowNumber);
	});

	m_downButton	= new QToolButton();
	m_downButton->setFocusPolicy(Qt::NoFocus);
//	m_downButton->setIcon(style()->standardIcon(QStyle::SP_ArrowDown));
	m_downButton->setIcon(QIcon("://img/down.png"));
	m_downButton->setToolTip(tr("Move Down"));
	m_vboxLayout->addWidget(m_downButton);
	connect(m_downButton, &QToolButton::clicked, [&](bool)
	{
		emit downClicked(m_rowNumber);
	});

	m_vboxLayout->addItem(new QSpacerItem(0, 1,
																				QSizePolicy::Minimum,
																				QSizePolicy::Expanding));
}
