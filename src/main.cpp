#include <gui/mainwindow.hpp>
#include <QApplication>
#include <QDir>

#include <kc_global_types.hpp>
#include <kc_global_funcs.hpp>
#include <kc_global_vars.hpp>

#include <QDebug>

int main(int argc, char *argv[])
{
	QApplication	a(argc, argv);

	g_faTr.load("kamand-composer_fa",
							QApplication::applicationDirPath() + "/languages");

	MainWindow		w;

	g_cfg	= new Config(QApplication::applicationDirPath() + QDir::separator() +
										 "kamand-composer.conf");
	g_cfg->load();

	QObject::connect(qApp, &QApplication::aboutToQuit,
									 [&]() { onAboutToQuit(&w); });

	w.loadConfigurationParameters();

	return a.exec();
}
