#include <kc_global_funcs.hpp>

#include <QMap>

#include <kc_global_vars.hpp>

#include <gui/mainwindow.hpp>

void onAboutToQuit(MainWindow* w)
{
	if (w)
	{
		w->saveConfigurationParameters();
	}

	g_cfg->save();
}

using Field2XmlStrMap	= QMap<Field, QString>;

static Field2XmlStrMap field2XmlStrMap()
{
	Field2XmlStrMap	m;

	m[Field::Section]				= "section";
	m[Field::Name]					= "name";
	m[Field::Synopsis]			= "synopsis";
	m[Field::Description]		= "description";
	m[Field::ReturnValue]		= "return-value";
	m[Field::Errors]				= "errors";
	m[Field::Attributes]		= "attributes";
	m[Field::Author]				= "author";
	m[Field::ConformingTo]	= "conforming-to";
	m[Field::Notes]					= "notes";
	m[Field::Bugs]					= "bugs";
	m[Field::ReportingBugs]	= "reporting-bugs";
	m[Field::SeeAlso]				= "see-also";
	m[Field::Colophon]			= "colophon";
	m[Field::Copyright]			= "copyright";
	m[Field::Keywords]			= "keywords";

	return m;
}

static Field2XmlStrMap	s_field2XmlStrMap	= field2XmlStrMap();

QString fieldToNodeName(Field f)
{
	return s_field2XmlStrMap.contains(f) ? s_field2XmlStrMap[f] : QString();
}

using XmlStr2FieldMap	= QMap<QString, Field>;

static XmlStr2FieldMap xmlStr2FieldMap()
{
	XmlStr2FieldMap	m;

	m["section"]				= Field::Section;
	m["name"]						= Field::Name;
	m["synopsis"]				= Field::Synopsis;
	m["description"]		= Field::Description;
	m["return-value"]		= Field::ReturnValue;
	m["errors"]					= Field::Errors;
	m["attributes"]			= Field::Attributes;
	m["author"]					= Field::Author;
	m["conforming-to"]	= Field::ConformingTo;
	m["notes"]					= Field::Notes;
	m["bugs"]						= Field::Bugs;
	m["reporting-bugs"]	= Field::ReportingBugs;
	m["see-also"]				= Field::SeeAlso;
	m["colophon"]				= Field::Colophon;
	m["copyright"]			= Field::Copyright;
	m["keywords"]				= Field::Keywords;

	return m;
}

static XmlStr2FieldMap	s_xmlStr2FieldMap	= xmlStr2FieldMap();

Field nodeNameToField(const QString& name)
{
	return s_xmlStr2FieldMap.contains(name.toLower()) ? s_xmlStr2FieldMap[name] : Field::Unknown;
}
