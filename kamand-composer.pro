#-------------------------------------------------
#
# Project created by QtCreator 2018-12-23T02:50:03
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = kamand-composer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
    src/main.cpp \
    src/gui/mainwindow.cpp \
    src/gui/mdichild.cpp \
    src/gui/updownbuttonswidget.cpp \
    src/gui/mainwindow_menu.cpp \
    src/gui/mainwindow_connections.cpp \
		src/gui/mainwindow_toolbar.cpp \
		src/gui/cpw/colorpickerwidget.cpp \
		src/gui/cpw/colors_html.cpp \
		src/gui/cpw/colorwidget.cpp \
		src/gui/cpw/colors_standard.cpp \
		src/gui/cpw/colors_tonal.cpp \
		src/gui/prefs/prefsdlg.cpp \
		src/gui/prefs/prefsbasewidget.cpp \
		src/gui/prefs/prefsgeneralwidget.cpp \
		src/gui/prefs/prefsuiwidget.cpp \
		src/gui/ew/editorwidget.cpp \
		src/gui/ew/editorwidget_connections.cpp \
		src/gui/ew/editorwidget_rows.cpp \
		src/gui/ew/editorwidget_xml.cpp \
		src/gui/ew/editorwidget_tr.cpp \
		src/gui/ew/editorwidget_menu.cpp \
		src/gui/about/aboutdialog.cpp \
		src/gui/about/aboutwidget.cpp \
		src/gui/about/licenseswidget.cpp \
    src/kc_global_types.cpp \
    src/kc_config.cpp \
    src/kc_global_funcs.cpp \
    src/kc_global_vars.cpp \
    src/gui/tbl/tablemakerwidget.cpp \
    src/gui/tbl/tablewidget.cpp \
    src/gui/tbl/tablepropsdialog.cpp \
		src/gui/tbl/tableinsertiondialog.cpp \
    src/gui/mainwindow_actions.cpp \
    src/gui/keywordswidget.cpp \
		src/gui/hyperlinkdialog.cpp

HEADERS += \
    inc/gui/mainwindow.hpp \
    inc/gui/mdichild.hpp \
    inc/gui/updownbuttonswidget.hpp \
		inc/gui/ew/editorwidget.hpp \
		inc/gui/cpw/colorpickerwidget.hpp \
		inc/gui/cpw/colors_globals.hpp \
		inc/gui/cpw/colorwidget.hpp \
		inc/gui/prefs/prefsdlg.hpp \
		inc/gui/prefs/prefsbasewidget.hpp \
		inc/gui/prefs/prefsgeneralwidget.hpp \
		inc/gui/prefs/prefsuiwidget.hpp \
		inc/gui/about/aboutdialog.hpp \
		inc/gui/about/aboutwidget.hpp \
		inc/gui/about/licenseswidget.hpp \
    inc/kc_global_vars.hpp \
    inc/kc_config.hpp \
    inc/kc_global_funcs.hpp \
    inc/kc_global_types.hpp \
    inc/gui/tbl/tablemakerwidget.hpp \
    inc/gui/tbl/tablewidget.hpp \
    inc/gui/tbl/tablepropsdialog.hpp \
		inc/gui/tbl/tableinsertiondialog.hpp \
    inc/gui/keywordswidget.hpp \
		inc/gui/hyperlinkdialog.hpp

FORMS += \
    frm/mainwindow.ui \
    frm/mdichild.ui \
    frm/prefsdlg.ui \
    frm/prefsgeneralwidget.ui \
    frm/prefsuiwidget.ui \
    frm/aboutdialog.ui \
    frm/aboutwidget.ui \
    frm/licenseswidget.ui \
    frm/tablepropsdialog.ui \
    frm/hyperlinkdialog.ui \
    frm/tableinsertiondialog.ui


RESOURCES += \
		res/kamand-composer.qrc

TRANSLATIONS += \
		lng/$${TARGET}_en.ts \
		lng/$${TARGET}_fa.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include(common.pri)
include(common_tr.pri)
